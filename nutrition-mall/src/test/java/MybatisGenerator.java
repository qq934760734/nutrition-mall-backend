import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.io.File;
import java.util.Arrays;


public class MybatisGenerator {

    public static void main(String[] args) {
        //1. 全局配置
        GlobalConfig config = new GlobalConfig();
        // 是否支持ActiveRecord模式
        config.setActiveRecord(false);
        // 作者
        config.setAuthor("zjn");
        // 生成路径
        config.setOutputDir(new File("nutrition-mall/src/main/java").getAbsolutePath());
        // 文件覆盖
        config.setFileOverride(true);
        // 设置生成的service接口的名字的首字母是否为I
        config.setBaseResultMap(true);
        config.setBaseColumnList(true);
        config.setOpen(false);
        config.setServiceName("%sDao");
        config.setServiceImplName("%sDaoImpl");

        //2. 数据源配置
        DataSourceConfig dsConfig = new DataSourceConfig();
        dsConfig.setDbType(DbType.MYSQL);
        dsConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dsConfig.setUrl("jdbc:mysql://localhost:3306/nutrition_mall?useSSL=true&verifyServerCertificate=false"
                + "&allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8");
        dsConfig.setUsername("root");
        dsConfig.setPassword("zhujienan2510");

        //3. 策略配置
        StrategyConfig stConfig = new StrategyConfig();
        //全局大写命名
        stConfig.setCapitalMode(true);
        // 数据库表映射到实体的命名策略
        stConfig.setNaming(NamingStrategy.underline_to_camel);
        // 需要生成的表
        stConfig.setInclude(new String[] {"platform_admin"});
        // 开启lombok模式
        stConfig.setEntityLombokModel(true);
        // 设置自动更新时间和自动设置创建时间
        stConfig.setTableFillList(Arrays.asList(new TableFill("create_time", FieldFill.INSERT)));

        //4. 包名策略配置
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent("com.example.nutritionmall");
        pkConfig.setMapper("mapper");
        pkConfig.setService("dao");
        pkConfig.setServiceImpl("dao.impl");
        pkConfig.setEntity("entity");
        pkConfig.setXml("mapper");

        //5. 排除不需要的模板
        TemplateConfig template = new TemplateConfig();
        template.setController(null);

        //6. 整合配置
        AutoGenerator ag = new AutoGenerator();
        ag.setGlobalConfig(config);
        ag.setDataSource(dsConfig);
        ag.setStrategy(stConfig);
        ag.setPackageInfo(pkConfig);
        ag.setTemplate(template);

        //7. 执行
        ag.execute();
    }

}
