package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("nm_evaluation")
@ApiModel(value = "nm_evaluation实体", description = "评价表")
public class NmEvaluation extends BaseEntity {
    @ApiModelProperty(value = "所属用户id")
    private String userId;
    @ApiModelProperty(value = "所属商品id")
    private String goodsId;
    @ApiModelProperty(value = "所属商店id")
    private String shopId;
    @ApiModelProperty(value = "所属订单项目id")
    private String ordersId;
    @ApiModelProperty(value = "评分，0.0-5.0")
    private Double score;
    @ApiModelProperty(value = "评价")
    private String evaluation;
    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}
