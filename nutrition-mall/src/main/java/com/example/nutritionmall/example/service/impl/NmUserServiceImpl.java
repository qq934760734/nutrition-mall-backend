package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.JwtUtil;
import com.example.nutritionmall.example.UserRequest;
import com.example.nutritionmall.example.entity.NmUser;
import com.example.nutritionmall.example.mapper.NmUserMapper;
import com.example.nutritionmall.example.service.NmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class NmUserServiceImpl extends ServiceImpl<NmUserMapper, NmUser> implements NmUserService {
    @Autowired
    private HttpServletRequest request;

    @Override
    public Boolean add(NmUser nmUser) {
        nmUser.setPermission(nmUser.getPermission());
        nmUser.setStatus(1);
        nmUser.setCreateTime(DateTool.getCurrTime());
        nmUser.setCreateBy(nmUser.getName());
        this.save(nmUser);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmUser nmUser) {
        this.updateById(nmUser);
        return true;
    }

    @Override
    public List<NmUser> list(String name) {
        QueryWrapper<NmUser> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", name);
        }
        return this.list(queryWrapper);
    }

    @Override
    public Page<NmUser> page(String name, String action, String permission, Integer pageNum, Integer pageSize) {
        Page<NmUser> page = new Page<>(pageNum, pageSize);
        QueryWrapper<NmUser> queryWrapper = new QueryWrapper<>();
        if (name != null && !name.isEmpty()) {
            if (action != null && action.equals("all")) {
                queryWrapper.and(wrapper -> wrapper.like("name", name).or().like("nickname", name));
            } else if (action != null) {
                queryWrapper.like(action, name);
            } else {
                queryWrapper.like("name", name);
            }
        }
        if (permission != null) {
            queryWrapper.eq("permission", Integer.parseInt(permission));
        }
        return this.page(page, queryWrapper);
    }

    @Override
    public NmUser getByName(String name) {
        QueryWrapper<NmUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name);
        return this.getOne(queryWrapper);
    }

    @Override
    public void updateUsefulByIds(String ids, int status) {
        String[] aryId = ids.split(",");
        for (String id : aryId) {
            NmUser nmUser = this.getById(id);
            if (nmUser != null) {
                nmUser.setStatus(status);
                this.updateById(nmUser);
            }
        }

    }

    @Override
    public void updatePermissionByIds(String ids, int permission) {
        String[] aryId = ids.split(",");
        for (String id : aryId) {
            NmUser nmUser = this.getById(id);
            if (nmUser != null) {
                nmUser.setPermission(permission);
                this.updateById(nmUser);
            }
        }
    }

    @Override
    public NmUser getByToken() {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        NmUser nmUser = getById(userId);
        return nmUser;
    }

    @Override
    public List<NmUser> listByNameAndPermission(String name, String permission) {
        QueryWrapper<NmUser> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", name);
        }
        if (permission != null) {
            queryWrapper.like("permission", permission);
        }
        return this.list(queryWrapper);
    }

    // 获取所有用户id的集合
    @Override
    public List<String> listAllUserId() {
        QueryWrapper<NmUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id");
        List<NmUser> userList = this.list(queryWrapper);
        List<String> userIdList = new ArrayList<>();
        for (NmUser nmUser : userList) {
            userIdList.add(nmUser.getId());
        }
        return userIdList;
    }

    /**
     * 根据请求搜索用户
     *
     * @param userRequest
     * @return
     */
    @Override
    public List<NmUser> listBySearch(UserRequest userRequest) {
        String id = userRequest.getUserId();
        String name = userRequest.getUsername();
        String permission = userRequest.getPermission();
        String telephone = userRequest.getTelephone();
        String nickname = userRequest.getNickname();
        log.warn(id + name + nickname + permission + telephone);
        return baseMapper.listBySearch(id, name, nickname, permission, telephone);
    }

}

