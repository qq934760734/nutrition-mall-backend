package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmGoodsCategory;

import java.util.List;

public interface NmGoodsCategoryService extends IService<NmGoodsCategory> {
    Boolean add(NmGoodsCategory NmGoodsCategory);

    void deleteByIds(String ids);

    Boolean update(NmGoodsCategory NmGoodsCategory);

    List<NmGoodsCategory> list(String name);

    List<NmGoodsCategory> listByUserId(String createBy);

    NmGoodsCategory isAlive(String name);

    List<NmGoodsCategory> listByName(String name);
}
