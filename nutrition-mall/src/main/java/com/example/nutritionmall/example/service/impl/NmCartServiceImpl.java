package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.JwtUtil;
import com.example.nutritionmall.example.entity.NmCart;
import com.example.nutritionmall.example.mapper.NmCartMapper;
import com.example.nutritionmall.example.service.NmCartService;
import com.example.nutritionmall.example.vo.CartGoods;
import com.example.nutritionmall.example.vo.CartGoodsShop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class NmCartServiceImpl extends ServiceImpl<NmCartMapper, NmCart> implements NmCartService {

    @Autowired
    private HttpServletRequest request;

    @Override
    public Boolean add(NmCart nmCart) {
        nmCart.setCreateTime(DateTool.getCurrTime());
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        nmCart.setCreateBy(userId);
        nmCart.setUserId(userId);
        NmCart cartExit = getByGoodsId(nmCart.getGoodsId());
        CartGoods cartGoods = baseMapper.getByGoodsId(userId, nmCart.getGoodsId());
        if (cartExit == null) {
            nmCart.setQuantity(1);
            this.save(nmCart);
            return true;
        }
        cartExit.setQuantity(cartExit.getQuantity() + 1);
        this.updateById(cartExit);
        return true;
    }

    @Override
    public Boolean deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        return this.removeByIds(listIds);
    }

    @Override
    public NmCart getByGoodsId(String goodsId) {
        QueryWrapper<NmCart> queryWrapper = new QueryWrapper<>();
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        queryWrapper.eq("user_id", userId);
        queryWrapper.eq("goods_id", goodsId);
        return this.getOne(queryWrapper);
    }

    @Override
    public Boolean update(NmCart nmCart) {
        this.updateById(nmCart);
        return true;
    }

    @Override
    public Page<CartGoods> page(String name, Integer pageNum, Integer pageSize) {
        Page<CartGoods> page = new Page<>(pageNum, pageSize);
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        return baseMapper.pageByGoodsName(page, userId, name);
    }


    @Override
    public List<CartGoods> listByGoodsName(String name) {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        return baseMapper.listByGoodsName(userId, name);
    }

    @Override
    public List<CartGoodsShop> listFullInfo() {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        return baseMapper.listCartGoodsShop(userId);
    }

    // 根据用户id查询该用户购物车中的商品id
    @Override
    public List<String> listGoodsIdsByUserId(String userId) {
        QueryWrapper<NmCart> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("goods_id");
        queryWrapper.eq("user_id", userId);
        List<NmCart> cartList = this.list(queryWrapper);
        // 去重，去除goodsIds内的重复项
        Set<String> goodsIds = new HashSet<>();
        for(NmCart nmCart : cartList) {
            goodsIds.add(nmCart.getGoodsId());
        }
        return new ArrayList<>(goodsIds);
    }


}