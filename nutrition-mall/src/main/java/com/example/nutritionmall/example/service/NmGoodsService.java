package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.ListByShopIdRequest;
import com.example.nutritionmall.example.entity.NmGoods;
import com.example.nutritionmall.example.vo.GoodsShop;

import java.util.List;
import java.util.Map;

public interface NmGoodsService extends IService<NmGoods> {

    Boolean add(NmGoods nmGoods);

    void deleteByIds(String ids);

    Boolean update(NmGoods nmGoods);

    List<GoodsShop> list(String id);

    List<GoodsShop> listBySecKill();

    List<GoodsShop> listByIds(String ids);

    List<GoodsShop> listByCategory(String category);

    List<GoodsShop> listByCategoryAndName(String shopId,String category, String name);

    List<NmGoods> listByShopId(ListByShopIdRequest request);

    List<NmGoods> goodsOrderByPriceDesc(String name);

    NmGoods getByName(String name);

    Map<String,List<NmGoods>> getGoodsByShopIds(String ids);

    List<String> checkStock(Map<String, Integer> stockMap);

    List<String> updateStockByMap(Map<String, Integer> stockMap, String operator);

    List<NmGoods> listByIdOrGoodsNameOrCategory(String goodsId, String goodsName, String category);

    Integer increaseViews(String goodsId);
    /**
     * 秒杀
     */
    List<Map<String,Object>> queryAllGoodStock();

    Map<String, Object> queryGoodStockById(Map<String, Object> m);

    List<Map<String, Object>> queryOrderByUserIdAndCoodsId(Map<String, Object> m);

    String getMiaoshaResult(String userid, String goodsId);

//    void miaosha(Map<String, Object> m);
}
