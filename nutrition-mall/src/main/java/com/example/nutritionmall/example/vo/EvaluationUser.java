package com.example.nutritionmall.example.vo;

import com.example.nutritionmall.example.entity.NmEvaluation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "EvaluationUser实体",description = "评价用户表")
public class EvaluationUser extends NmEvaluation {
    @ApiModelProperty(value = "用户昵称")
    private String userNickname;
    @ApiModelProperty(value = "用户头像路径")
    private String userImage;
}
