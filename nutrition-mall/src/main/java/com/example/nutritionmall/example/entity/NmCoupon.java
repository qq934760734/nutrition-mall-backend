package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("nm_coupon")
@ApiModel(value = "nm_coupon实体", description = "优惠券表")
public class NmCoupon extends BaseEntity {

    @ApiModelProperty(value = "可以选择“店铺id”，“商品id”，空的时候表示任何商品都可以使用这个优惠券")
    private String contentId;
    @ApiModelProperty(value = "可以选择“商品优惠券”“店铺优惠券”等")
    private String type;
    @ApiModelProperty(value = "折扣的方式（现金券，折扣券，满减券，礼品券）")
    private String methods;
    @ApiModelProperty(value = "优惠券名称")
    private String name;
    @ApiModelProperty(value = "满减条件（condition是关键字），x\n" +
            "当value_desc=“元”时，内容为\n" +
            "满 condition_value 元能优惠value，\n" +
            "当value_desc=\"折\"时，内容为\n" +
            "满condition_value 元，最多能优惠多少元")
    private String conditions;
    @ApiModelProperty(value = "达到多少元门槛可以使用")
    private String conditionsValue;
    @ApiModelProperty(value = "开始时间")
    private String startTime;
    @ApiModelProperty(value = "结束时间")
    private String endTime;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "不可用时展示原因")
    private String reason;
    @ApiModelProperty(value = "折扣（9.88折）")
    private BigDecimal discount;
    @ApiModelProperty(value = "打折金额")
    private BigDecimal value;
    @ApiModelProperty(value = "打折时，最高折扣金额")
    private BigDecimal valueMax;
    @ApiModelProperty(value = "优惠文案")
    private String valueDesc;
    @ApiModelProperty(value = "价格单位(元、折)")
    private String unitDesc;
    @ApiModelProperty(value = "使用次数")
    private int usageCount;
    @ApiModelProperty(value = "最大使用次数")
    private int maxUsage;
    @ApiModelProperty(value = "状态：-1-已禁用 0-可领取，1-已过期，2-已抢光")
    private int status;
    @ApiModelProperty(value = "创建人")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}
