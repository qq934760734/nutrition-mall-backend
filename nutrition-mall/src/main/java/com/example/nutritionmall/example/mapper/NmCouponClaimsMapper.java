package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmCouponClaims;
import com.example.nutritionmall.example.vo.CouponClaimsCoupon;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NmCouponClaimsMapper extends BaseMapper<NmCouponClaims> {
        @Select("SELECT c2.id as `id`,c2.coupon_id as `couponId`,c2.user_id as `userId`,c2.orders_id as `ordersId`,c2.claim_date as `claimDate`,c2.claim_status as `claimStatus`,c1.name as `name`,c1.conditions as `conditions`,c1.conditions_value as `conditionsValue`,c1.methods as `methods`,c1.type as `type`,c1.content_id as `contentId`,c1.unit_desc as `unitDesc`,c1.value_desc as `valueDesc`,c1.value_max as `valueMax`,c1.discount as `discount`,c1.start_time as `startTime`,c1.end_time as `endTime`,c1.description as `description`,c1.reason as `reason`,c1.status as `status`,c1.max_usage as `maxUsage`,c1.usage_count as `usageCount`" +
            "FROM nm_coupon c1, nm_coupon_claims c2 " +
            "WHERE c1.id = c2.coupon_id AND c2.user_id = '${userId}' " +
            "ORDER BY c2.create_time DESC")
    List<CouponClaimsCoupon> listByUserId(@Param("userId") String userId);
}
