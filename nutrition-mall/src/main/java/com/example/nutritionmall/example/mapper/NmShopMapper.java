package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmShop;

public interface NmShopMapper extends BaseMapper<NmShop> {
}
