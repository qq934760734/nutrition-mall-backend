package com.example.nutritionmall.example.vo;

import com.example.nutritionmall.example.entity.NmCart;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "CartGoodsShop实体",description = "购物车商品商店表")
public class CartGoodsShop extends NmCart {
    @ApiModelProperty(value = "商品名")
    private String name;
    @ApiModelProperty(value = "单价")
    private String price;
    @ApiModelProperty(value = "图片路径")
    private String image;
    @ApiModelProperty(value = "类别")
    private String category;
    @ApiModelProperty(value = "标签")
    private String tags;
    @ApiModelProperty(value = "商店id")
    private String ShopId;
    @ApiModelProperty(value = "商店名")
    private String ShopName;
    @ApiModelProperty(value = "商店图片")
    private String ShopImage;
}
