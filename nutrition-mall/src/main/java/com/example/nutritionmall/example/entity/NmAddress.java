package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("nm_address")
@ApiModel(value = "nm_address实体", description = "地址表")
public class NmAddress extends BaseEntity {
    @ApiModelProperty(value = "所属用户id，关联user表")
    private String userId;
    @ApiModelProperty(value = "姓名")
    private String name;
    @ApiModelProperty(value = "电话")
    private String telephone;
    @ApiModelProperty(value = "省")
    private String province;
    @ApiModelProperty(value = "市")
    private String city;
    @ApiModelProperty(value = "区")
    private String area;
    @ApiModelProperty(value = "地区编号")
    private String areaCode;
    @ApiModelProperty(value = "邮政编码")
    private String postalCode;
    @ApiModelProperty(value = "详细地址")
    private String detailAddress;
    @ApiModelProperty(value = "是否为默认地址，0-否，1-是")
    private Boolean isDefault;

    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;


}