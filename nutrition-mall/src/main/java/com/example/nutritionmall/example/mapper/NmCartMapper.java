package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.nutritionmall.example.entity.NmCart;
import com.example.nutritionmall.example.vo.CartGoods;
import com.example.nutritionmall.example.vo.CartGoodsShop;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NmCartMapper extends BaseMapper<NmCart> {
    //列名要用`括起来
    @Select("SELECT c.*, g.name as `name`, g.price as `price`, g.image as `image` ,g.tags as `tags`" +
            "FROM nm_cart c, nm_goods g " +    //！！行末有空格，否则变成sWHERE
            "WHERE c.user_id = '${userId}' AND c.goods_id = g.id AND g.name LIKE '%${goodsName}%' " +
            "ORDER BY c.create_time DESC")
    List<CartGoods> listByGoodsName(@Param("userId") String userId, @Param("goodsName") String goodsName);

    @Select("SELECT c.*, g.name as `name`,  g.price as `price`, g.image as `image` " +
            "FROM nm_cart c, nm_goods g " +
            "WHERE c.user_id = '${userId}' AND c.goods_id = g.id AND g.name LIKE '%${goodsName}%' " +
            "ORDER BY c.create_time DESC")
    Page<CartGoods> pageByGoodsName(@Param("page") Page<CartGoods> page, @Param("userId") String userId, @Param("goodsName") String goodsName);


    @Select("SELECT c.*, g.name as `name`, g.price as `price`, g.image as `image`, g.category as `category`, g.tags as `tags`, " +
            "s.id as `shop_id`, s.name as `shop_name`, s.image as `shop_image` " +
            "FROM nm_cart c, nm_goods g, nm_shop s " +
            "WHERE c.user_id = '${userId}' AND c.goods_id = g.id AND g.shop_id = s.id " +
            "ORDER BY c.create_time DESC")
    List<CartGoodsShop> listCartGoodsShop(@Param("userId") String userId);

    @Select("SELECT c.*, g.name as `name`, g.price as `price`, g.image as `image`, g.category as `category`, g.tags as `tags` " +
            "FROM nm_cart c, nm_goods g " +
            "WHERE c.user_id = '${userId}' AND c.goods_id = '${goodsId}' AND c.goods_id = g.id ")
    CartGoods getByGoodsId(@Param("userId") String userId, @Param("goodsId") String goodsId);
}