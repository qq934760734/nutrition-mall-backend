package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmImages;

import java.util.List;

public interface NmImagesService extends IService<NmImages> {
    Boolean add(NmImages nmImages);

    void deleteByIds(String ids);

    Boolean update(NmImages nmImages);

    List<NmImages> getImagesByGoodsId(String goodsId);
}
