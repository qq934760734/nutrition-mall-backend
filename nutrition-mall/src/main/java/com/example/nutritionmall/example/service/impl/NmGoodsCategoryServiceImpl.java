package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.example.entity.NmGoodsCategory;
import com.example.nutritionmall.example.mapper.NmGoodsCategoryMapper;
import com.example.nutritionmall.example.service.NmGoodsCategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NmGoodsCategoryServiceImpl extends ServiceImpl<NmGoodsCategoryMapper, NmGoodsCategory> implements NmGoodsCategoryService {

    @Override
    public Boolean add(NmGoodsCategory nmGoodsCategory) {
        nmGoodsCategory.setCreateTime(DateTool.getCurrTime());
        this.save(nmGoodsCategory);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmGoodsCategory nmGoodsCategory) {
        this.updateById(nmGoodsCategory);
        return true;
    }

    @Override
    public List<NmGoodsCategory> list(String name) {
        QueryWrapper<NmGoodsCategory> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", name);
        }
        return this.list(queryWrapper);
    }

    @Override
    public List<NmGoodsCategory> listByUserId(String createBy) {
        QueryWrapper<NmGoodsCategory> queryWrapper = new QueryWrapper<>();
        if (createBy != null) {
            queryWrapper.like("create_by", createBy);
        }
        return this.list(queryWrapper);
    }

    /**
     * 判断是否本人已经创建该分类
     * @param name
     * @return
     */
    @Override
    public NmGoodsCategory isAlive(String name) {

        QueryWrapper<NmGoodsCategory> queryWrapper = new QueryWrapper<>();
        if(name!=null){
            queryWrapper.eq("name", name);
        }
        return this.getOne(queryWrapper);
    }

    @Override
    public List<NmGoodsCategory> listByName(String name) {
        QueryWrapper<NmGoodsCategory> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", "%"+name+"%");
        }
        return this.list(queryWrapper);
    }
}
