package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("nm_coupon_claims")
@ApiModel(value = "nm_coupon_claims实体", description = "优惠券领取表")
public class NmCouponClaims extends BaseEntity {

    @ApiModelProperty(value = "关联优惠券表")
    private String couponId;
    @ApiModelProperty(value = "关联用户表")
    private String userId;
    @ApiModelProperty(value = "关联订单表，如有")
    private String ordersId;
    @ApiModelProperty(value = "领取时间")
    private String claimDate;
    @ApiModelProperty(value = "状态： 0-未使用，1-已过期，2-已使用")
    private int claimStatus;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}
