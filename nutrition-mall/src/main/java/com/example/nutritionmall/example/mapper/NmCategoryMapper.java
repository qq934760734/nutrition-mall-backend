package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmCategory;
import com.example.nutritionmall.example.vo.CartGoods;
import com.example.nutritionmall.example.vo.OrdersGoodsShop;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NmCategoryMapper extends BaseMapper<NmCategory> {

}
