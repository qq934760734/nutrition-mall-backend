package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmCoupon;
import com.example.nutritionmall.example.service.NmCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/coupon")
@Api(tags = "NmCouponController", description = "优惠券controller，返回的baseUrl=http://localhost:8998/nutritionMall/")
public class NmCouponController {
    @Resource
    private NmCouponService nmCouponService;

    /**
     * 添加优惠券
     *
     * @param nmCoupon
     * @return
     */
    @ApiOperation(value = "添加优惠券")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmCoupon nmCoupon) {
        Result result = new Result();
        //是否存在相同名字 同一商家不能有两个同名优惠券
        NmCoupon couponExit = nmCouponService.getByName(nmCoupon.getName(),nmCoupon.getCreateBy());
        if (couponExit != null) {
            result.fail("优惠券名字已存在");
            return result;
        }
        nmCouponService.add(nmCoupon);
        result.success("添加成功");
        result.setData(nmCoupon);
        return result;
    }


    /**
     * 查询商家优惠券信息
     *
     * @param
     * @return
     */
    @ApiOperation(value = "查询商家优惠券信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", paramType = "query", value = "用户id，可为空")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list(String userId) {
        Result result = new Result();
        result.setData(nmCouponService.listByUserId(userId));
        result.success("查询list成功");
        return result;
    }

    /**
     * 查询商家优惠券信息
     *
     * @param
     * @return
     */
    @ApiOperation(value = "查询商家优惠券信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ShopId", paramType = "query", value = "店铺id，可为空")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/listByShopId")
    public Result listByShopId(String shopId) {
        log.warn("shopId:" + shopId);
        Result result = new Result();
        result.setData(nmCouponService.listByShopId(shopId));
        result.success("查询list成功");
        return result;
    }

    /**
     * 更新
     *
     * @param nmCoupon
     * @return
     */
    //
    @ApiOperation(value = "修改优惠券信息")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmCoupon nmCoupon) {
        Result result = new Result();
        nmCouponService.update(nmCoupon);
        result.success("修改成功");
        return result;
    }

    /**
     * 批量删除优惠券种类-deleteByIds
     *
     * @param ids
     * @return
     */
    @ApiOperation(value = "批量删除优惠券种类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmCouponService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    /**
     * 查询优惠券信息
     *
     * @param name
     * @return
     */
    @ApiOperation(value = "查询优惠券信息（可指定name）")
    @RequestMapping(method = RequestMethod.POST, value = "/listByName")
    public Result listByName(String name) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmCouponService.listByName(name));
        return result;
    }
}
