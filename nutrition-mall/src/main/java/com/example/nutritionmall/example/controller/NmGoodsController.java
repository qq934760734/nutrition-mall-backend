package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.ListByShopIdRequest;
import com.example.nutritionmall.example.entity.NmGoods;
import com.example.nutritionmall.example.service.NmGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/goods")
@Api(tags = "NmGoodsController", description = "商品controller，返回的baseUrl=http://localhost:8998/nutritionMall/")
public class NmGoodsController {

    @Resource
    private NmGoodsService nmGoodsService;

    /**
     * 添加商品，可以同名
     *
     * @param nmGoods
     * @return
     */
    //添加商品
    @ApiOperation(value = "添加商品")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmGoods nmGoods) {
        Result result = new Result();
//        NmGoods goodsNameExit = nmGoodsService.getByName(nmGoods.getName());
//        if (goodsNameExit != null) {
//            result.fail("商品名称：" + nmGoods.getName() + "已存在");
//        } else {
//            if (nmGoods.getShopId() != null) {
//                nmGoodsService.add(nmGoods);
//                result.success("添加成功");
//                result.setData(nmGoods);
//            } else {
//                result.fail("添加失败，请返回重试");
//            }
//        }
        if (nmGoods.getShopId() != null) {
            nmGoodsService.add(nmGoods);
            result.success("添加成功");
            result.setData(nmGoods);
        } else {
            result.fail("添加失败，请返回重试");
        }
        return result;
    }

    //批量删除商品-deleteByIds
    @ApiOperation(value = "批量删除商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmGoodsService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    //修改商品信息
    @ApiOperation(value = "修改商品信息")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmGoods nmGoods) {
        Result result = new Result();
        nmGoodsService.update(nmGoods);
        result.success("修改成功");
        return result;
    }

    //查询所有商品信息和其所属商店信息
    @ApiOperation(value = "查询一定价格内的所有商品信息和其所属商店信息（可指定name）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", paramType = "query", value = "商品名关键字，可以为空"),
            @ApiImplicitParam(name = "priceMin", paramType = "query", value = "价格最小值，可以为空"),
            @ApiImplicitParam(name = "priceMax", paramType = "query", value = "价格最大值，可以为空"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list(String id) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsService.list(id));
        return result;
    }

    //查询秒杀类商品信息
    @ApiOperation(value = "查询一定价格内的所有商品信息和其所属商店信息（可指定name）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", paramType = "query", value = "商品名关键字，可以为空"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/listBySecKill")
    public Result listBySecKill() {
        Result result = new Result();
        result.success("查询所有秒杀类商品成功");
        result.setData(nmGoodsService.listBySecKill());
        return result;
    }

    //通过ids查询商店
    @ApiOperation(value = "通过商品ids查询商品和商店信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", paramType = "query", required = true, value = "商品id集合，用逗号,隔开"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/listByIds")
    public Result listByIds(String ids) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsService.listByIds(ids));
        return result;
    }

    //根据category查询商品信息
    @ApiOperation(value = "根据category查询商品信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", paramType = "query", value = "商品名关键字，可以为空"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/listByCategory")
    public Result listByCategory(String category) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsService.listByCategory(category));
        return result;
    }

    //根据category和name查询商品信息
    @ApiOperation(value = "根据category和name查询商品信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", paramType = "query", value = "商品名关键字，可以为空"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/listByCategoryAndName")
    public Result listByCategoryAndName(String shopId, String category, String name) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsService.listByCategoryAndName(shopId, category, name));
        return result;
    }

    //查询指定店铺的所有商品信息（可指定name）
//    @ApiOperation(value = "查询指定店铺的所有商品信息")
//    @RequestMapping(method = RequestMethod.POST, value = "/listByShopId")
//    public Result listByShopId(String shopId) {
//
//        Result result = new Result();
//        result.success("查询list成功");
//        result.setData(nmGoodsService.listByShopId(shopId));
//        return result;
//    }
    @ApiOperation(value = "查询指定店铺的所有商品信息")
    @RequestMapping(method = RequestMethod.POST, value = "/listByShopId")
    public Result listByShopId(@RequestBody ListByShopIdRequest request) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsService.listByShopId(request));
        return result;
    }


    /**
     * 模糊搜索商品名称，并按照价格降序排序
     *
     * @param name
     * @return
     */
    @ApiOperation(value = "根据category和name查询商品信息")
    @RequestMapping(method = RequestMethod.POST, value = "/goodsOrderByPriceDesc")
    public Result goodsOrderByPriceDesc(String name) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsService.goodsOrderByPriceDesc(name));
        return result;
    }

    /**
     * 获取某商店id的商品
     *
     * @param shopIds
     * @return
     */
    @ApiOperation(value = "获取某商店id的商品")
    @RequestMapping(method = RequestMethod.POST, value = "/getGoodsByShopIds")
    public Result getGoodsByShopIds(String shopIds) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsService.getGoodsByShopIds(shopIds));
        return result;
    }

    /**
     * 检查商品库存，返回库存不足的商品名列表
     *
     * @param stockMap
     * @return
     */
    @ApiOperation(value = "检查商品库存，返回库存不足的商品名列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "stockMap", required = true, paramType = "body",
                    value = "商品id、库存扣减数量quantity组成的键值对"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/checkStock")
    public Result checkStock(@RequestBody Map<String, Integer> stockMap) {
        Result result = new Result();
        List<String> names = nmGoodsService.checkStock(stockMap);
        if (names.isEmpty()) {
            result.success("库存充足");
        } else {
            result.success("商品库存不足");
            result.setData(names);
        }
        return result;
    }

    /**
     * 更新商品库存，返回库存不足的商品名
     *
     * @param stockMap
     * @param operator
     * @return
     */
    @ApiOperation(value = "更新商品库存，返回库存不足的商品名列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "stockMap", required = true, paramType = "body",
                    value = "商品id、库存增减数量quantity组成的键值对"),
            @ApiImplicitParam(name = "operator", paramType = "query", value = "操作符，可选reduce/add"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/updateStockByMap")
    public Result updateStockByMap(@RequestBody Map<String, Integer> stockMap, String operator) {
        Result result = new Result();
        // 默认操作为减库存
        String op = "reduce";
        if (operator != null) {
            op = operator;
        }
        List<String> names = nmGoodsService.updateStockByMap(stockMap, op);
        if (names.isEmpty()) {
            result.success("更新成功");
        } else {
            result.fail("存在商品库存不足或商品不存在");
            result.setData(names);
        }
        return result;
    }

    /**
     * 根据商品id或商品名或商品分类来搜索商品
     *
     * @param goodsId,goodsName,category
     * @return
     */
    @ApiOperation(value = "根据订单id或收货人姓名或订单的状态搜索")
    @RequestMapping(method = RequestMethod.POST, value = "/listByIdOrGoodsNameOrCategory")
    public Result listByIdOrGoodsNameOrCategory(String goodsId, String goodsName, String category) {
        Result result = new Result();
        result.setData(nmGoodsService.listByIdOrGoodsNameOrCategory(goodsId, goodsName, category));
        result.success("查询list成功");
        return result;
    }

    /**
     * 增加一次指定商品的浏览量
     *
     * @param goodsId
     * @return
     */
    @ApiOperation(value = "增加一次指定商品的浏览量")
    @ApiImplicitParam(name = "goodsId", paramType = "query", required = true, value = "商品id")
    @RequestMapping(method = RequestMethod.POST, value = "/increaseViews")
    public Result increaseViews(String goodsId) {
        Result result = new Result();
        Integer views = nmGoodsService.increaseViews(goodsId);
        result.success("增加浏览量成功");
        Map map = new HashMap<>();
        map.put(goodsId, views);
        result.setData(map);
        return result;
    }


}

