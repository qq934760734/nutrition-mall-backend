package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmCart;
import com.example.nutritionmall.example.vo.CartGoods;
import com.example.nutritionmall.example.vo.CartGoodsShop;

import java.util.List;

public interface NmCartService extends IService<NmCart> {
    Boolean add(NmCart nmCart);

    Boolean deleteByIds(String ids);

    NmCart getByGoodsId(String goodsId);

    Boolean update(NmCart nmCart);

    Page<CartGoods> page(String goodsName, Integer pageNum, Integer pageSize);

    List<CartGoods> listByGoodsName(String name);

    List<CartGoodsShop> listFullInfo();

    List<String> listGoodsIdsByUserId(String userId);
}
