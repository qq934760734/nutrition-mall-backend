package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmVideo;
import com.example.nutritionmall.example.service.NmVideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/video")
@Api(tags = "NmVideoController", description = "视频controller，返回的baseUrl=http://localhost:8998/bookStore/")
public class NmVideoController {

    @Autowired
    private NmVideoService nmVideoService;

    //添加视频
    @ApiOperation(value = "添加视频")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmVideo nmVideo) {
        Result result = new Result();
        if (nmVideo.getGoodsId() != null) {
            nmVideoService.add(nmVideo);
            result.success("添加成功");
            result.setData(nmVideo);
        } else {
            result.fail("添加失败，请返回重试");
        }
        return result;
    }

    //批量删除视频-deleteByIds
    @ApiOperation(value = "批量删除视频")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmVideoService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    //修改视频
    @ApiOperation(value = "修改视频")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmVideo nmVideo) {
        Result result = new Result();
        nmVideoService.update(nmVideo);
        result.success("修改成功");
        return result;
    }

    //查询所有视频信息（可指定name）
    @ApiOperation(value = "查询所有视频信息（可指定name）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", paramType = "query", value = "视频名关键字，可以为空"),
            @ApiImplicitParam(name = "priceMin", paramType = "query", value = "价格最小值，可以为空"),
            @ApiImplicitParam(name = "priceMax", paramType = "query", value = "价格最大值，可以为空"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list(String name, Double priceMin, Double priceMax) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmVideoService.list(name, priceMin, priceMax));
        return result;
    }

    //查询指定店铺的所有视频信息（可指定name）
    @ApiOperation(value = "查询指定店铺的所有视频信息（可指定name）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", paramType = "query", required = true, value = "店铺id"),
            @ApiImplicitParam(name = "name", paramType = "query", value = "视频名关键字，可以为空"),
            @ApiImplicitParam(name = "priceMin", paramType = "query", value = "价格最小值，可以为空"),
            @ApiImplicitParam(name = "priceMax", paramType = "query", value = "价格最大值，可以为空"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/listByShopId")
    public Result listByShopId(String shopId, String name, Double priceMin, Double priceMax) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmVideoService.listByShopId(shopId, name, priceMin, priceMax));
        return result;
    }



    //查-getById
    @ApiOperation(value = "通过id查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true, paramType = "query", value = "需要查询的id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getById")
    public Result getById(String id) {
        Result result = new Result();

        NmVideo nmVideo = nmVideoService.getById(id);
        if (nmVideo != null) {
            result.success("获取成功");
            result.setData(nmVideo);
        } else {
            result.fail("获取失败");
        }

        return result;
    }

    //根据goodsId查询video
    @ApiOperation(value = "通过goodsId查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsId", required = true, paramType = "query", value = "需要查询的商品id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getVideoByGoodsId")
    public Result getVideoByGoodsId(String goodsId) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmVideoService.getVideoByGoodsId(goodsId));
        return result;
    }

}
