package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.JwtUtil;
import com.example.nutritionmall.example.entity.NmAddress;
import com.example.nutritionmall.example.mapper.NmAddressMapper;
import com.example.nutritionmall.example.service.NmAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class NmAddressServiceImpl extends ServiceImpl<NmAddressMapper, NmAddress> implements NmAddressService {

    @Autowired
    private HttpServletRequest request;

    @Override
    public Boolean add(NmAddress address) {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        address.setUserId(userId);
        address.setCreateTime(DateTool.getCurrTime());
        //取消原默认地址
        if (address.getIsDefault()) {
            QueryWrapper<NmAddress> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("is_default", true);
            queryWrapper.eq("user_id", userId);
            NmAddress oldAddress = this.getOne(queryWrapper);
            if (oldAddress != null) {
                oldAddress.setIsDefault(false);
                update(oldAddress);
            }
        }
        this.save(address);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmAddress address) {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        //取消原默认地址
        if (address.getIsDefault()) {
            QueryWrapper<NmAddress> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("is_default", true);
            queryWrapper.eq("user_id", userId);
            NmAddress oldAddress = this.getOne(queryWrapper);
            if (oldAddress != null) {
                oldAddress.setIsDefault(false);
                this.update(oldAddress);
            }
        }
        this.updateById(address);
        return true;
    }

    @Override
    public List<NmAddress> list(String userId, Boolean isDefault) {
        QueryWrapper<NmAddress> queryWrapper = new QueryWrapper<>();
        if (userId == null || userId.isEmpty()) {
            String token = request.getHeader("Authorization");
            userId = JwtUtil.validateToken(token);
        }
        queryWrapper.eq("user_id", userId);
        if (isDefault != null) {
            queryWrapper.eq("is_default", true);
        }
        return this.list(queryWrapper);
    }
}