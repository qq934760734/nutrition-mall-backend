package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmVideo;

import java.util.List;

public interface NmVideoService extends IService<NmVideo> {
    Boolean add(NmVideo nmVideo);

    void deleteByIds(String ids);

    Boolean update(NmVideo nmVideo);

    List<NmVideo> list(String name, Double priceMin, Double priceMax);

    List<NmVideo> listByShopId(String shopId, String name, Double priceMin, Double priceMax);

    List<NmVideo> getVideoByGoodsId(String goodsId);
}
