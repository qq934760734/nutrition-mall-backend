package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.UserRequest;
import com.example.nutritionmall.example.entity.NmUser;

import java.util.List;

public interface NmUserService extends IService<NmUser> {
    Boolean add(NmUser user);

    void deleteByIds(String ids);

    Boolean update(NmUser user);

    List<NmUser> list(String name);

    Page<NmUser> page(String name, String action, String permission, Integer pageNum, Integer pageSize);

    NmUser getByName(String name);

    void updateUsefulByIds(String ids, int status);

    void updatePermissionByIds(String ids, int permission);

    NmUser getByToken();

    List<NmUser> listByNameAndPermission(String name, String permission);

    List<String> listAllUserId();

    List<NmUser> listBySearch(UserRequest userRequest);
}