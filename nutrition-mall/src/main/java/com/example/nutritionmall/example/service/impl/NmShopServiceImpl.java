package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.JwtUtil;
import com.example.nutritionmall.example.entity.NmShop;
import com.example.nutritionmall.example.mapper.NmShopMapper;
import com.example.nutritionmall.example.service.NmShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class NmShopServiceImpl extends ServiceImpl<NmShopMapper, NmShop> implements NmShopService {

    @Autowired
    private HttpServletRequest request;

    @Override
    public Boolean add(NmShop nmShop) {
        String token = request.getHeader("Authorization");
        String ownerId = JwtUtil.validateToken(token);
        nmShop.setOwnerId(ownerId);
        nmShop.setCreateTime(DateTool.getCurrTime());
        this.save(nmShop);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmShop nmShop) {
        this.updateById(nmShop);
        return true;
    }

    @Override
    public List<NmShop> list(String name) {
        QueryWrapper<NmShop> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", name);
        }
        queryWrapper.orderByDesc("create_time");
        return this.list(queryWrapper);
    }

    @Override
    public Page<NmShop> page(String adminId, String name, Integer pageNum, Integer pageSize) {
        Page<NmShop> page = new Page<>(pageNum, pageSize);
        QueryWrapper<NmShop> queryWrapper = new QueryWrapper<>();
        if (adminId == null || adminId.isEmpty()) {
            String token = request.getHeader("Authorization");
            String ownerId = JwtUtil.validateToken(token);
            queryWrapper.eq("owner_id", ownerId);
        } else {
            queryWrapper.eq("owner_id", adminId);
        }
        if (name != null) {
            queryWrapper.like("name", name);
        }
        queryWrapper.orderByDesc("create_time");
        return this.page(page, queryWrapper);
    }

    @Override
    public List<NmShop> listByOwnerId(String ownerId, String name) {
        QueryWrapper<NmShop> queryWrapper = new QueryWrapper<>();
        if (ownerId != null) {
            queryWrapper.eq("owner_id", ownerId);
        }
        if (name != null){
            queryWrapper.like("name",name);
        }
        queryWrapper.orderByDesc("create_time");
        return this.list(queryWrapper);
    }


}
