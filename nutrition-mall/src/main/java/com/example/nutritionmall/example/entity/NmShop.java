package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("nm_shop")
@ApiModel(value = "nm_shop实体", description = "商店表")
public class NmShop extends BaseEntity {
    @ApiModelProperty(value = "商店名称")
    private String name;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "图片")
    private String image;
    @ApiModelProperty(value = "视频")
    private String video;
    @ApiModelProperty(value = "商店位置，经度")
    private String longitude;
    @ApiModelProperty(value = "商店位置，纬度")
    private String latitude;
    @ApiModelProperty(value = "具体位置")
    private String location;
    @ApiModelProperty(value = "商店id")
    private String ownerId;

    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}
