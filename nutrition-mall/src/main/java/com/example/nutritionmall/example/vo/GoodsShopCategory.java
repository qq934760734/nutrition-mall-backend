package com.example.nutritionmall.example.vo;

import com.example.nutritionmall.example.entity.NmGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "GoodsShopCategory实体",description = "商品商店分类表")
public class GoodsShopCategory extends NmGoods {
    @ApiModelProperty(value = "店铺名称")
    private String shopName;
    @ApiModelProperty(value = "店铺图片路径")
    private String shopImage;

    @ApiModelProperty(value = "店铺图片路径")
    private String goodsCategory;
}