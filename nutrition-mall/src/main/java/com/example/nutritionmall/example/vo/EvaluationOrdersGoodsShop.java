package com.example.nutritionmall.example.vo;

import com.example.nutritionmall.example.entity.NmEvaluation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "EvaluationOrdersGoodsShop实体",description = "评价订单商品商店表")
public class EvaluationOrdersGoodsShop extends NmEvaluation {
    @ApiModelProperty(value = "商品Id")
    private String goodsId;
    @ApiModelProperty(value = "商品名")
    private String goodsName;
    @ApiModelProperty(value = "商品分类")
    private String category;
    @ApiModelProperty(value = "商品图片路径")
    private String goodsImage;
    @ApiModelProperty(value = "店铺id")
    private String shopId;
    @ApiModelProperty(value = "店铺名称")
    private String shopName;
    @ApiModelProperty(value = "店铺图片路径")
    private String shopImage;
    @ApiModelProperty(value = "单价")
    private double price;
    @ApiModelProperty(value = "数量")
    private int quantity;
    @ApiModelProperty(value = "总价")
    private double totalPrice;

    @ApiModelProperty(value = "店铺名称")
    private int status;
    @ApiModelProperty(value = "收件人")
    private String consigneeName;
    @ApiModelProperty(value = "电话")
    private String telephone;
    @ApiModelProperty(value = "地址")
    private String address;
    @ApiModelProperty(value = "商品详情")
    private String goodsDescription;
    @ApiModelProperty(value = "订单创建时间")
    private String createTime;
    @ApiModelProperty(value = "订单更新时间")
    private String updateTime;
}
