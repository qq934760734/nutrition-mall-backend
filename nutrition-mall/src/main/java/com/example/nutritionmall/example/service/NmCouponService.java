package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmCoupon;

import java.util.List;

public interface NmCouponService extends IService<NmCoupon> {

    Boolean add(NmCoupon nmCoupon);

    NmCoupon getByName(String name,String createBy);

    List<NmCoupon> listByUserId(String userId);

    List<NmCoupon> listByShopId(String shopId);

    Boolean update(NmCoupon nmCoupon);

    void deleteByIds(String ids);

    List<NmCoupon> listByName(String name);

}
