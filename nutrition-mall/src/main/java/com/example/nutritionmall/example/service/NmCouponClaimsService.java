package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmCoupon;
import com.example.nutritionmall.example.entity.NmCouponClaims;
import com.example.nutritionmall.example.vo.CouponClaimsCoupon;

import java.util.List;

public interface NmCouponClaimsService extends IService<NmCouponClaims> {

    Boolean add(NmCouponClaims nmCouponClaims);

    NmCouponClaims isExit(String userId,String couponId);

//    List<NmCouponClaims> listByUserId(String userId);

    List<CouponClaimsCoupon> list(String userId);
}
