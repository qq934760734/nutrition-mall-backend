package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmEvaluation;
import com.example.nutritionmall.example.vo.EvaluationOrdersGoodsShop;
import com.example.nutritionmall.example.vo.EvaluationUser;

import java.util.List;

public interface NmEvaluationService extends IService<NmEvaluation> {
    Boolean add(NmEvaluation nmEvaluation);

    void deleteByIds(String ids);

    Boolean update(NmEvaluation nmEvaluation);

    List<EvaluationOrdersGoodsShop> listByUserId();

    Page<EvaluationUser> pageByGoodsId(String goodsId, Integer pageNum, Integer pageSize);

    NmEvaluation getByOrdersId(String ordersId);
}
