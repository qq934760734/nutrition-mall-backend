package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.JwtUtil;
import com.example.nutritionmall.example.entity.NmCoupon;
import com.example.nutritionmall.example.mapper.NmCouponMapper;
import com.example.nutritionmall.example.service.NmCouponService;
import com.example.nutritionmall.example.service.NmShopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class NmCouponServiceImpl extends ServiceImpl<NmCouponMapper, NmCoupon> implements NmCouponService {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private NmShopService nmShopService;

    @Override
    public Boolean add(NmCoupon nmCoupon) {
        nmCoupon.setCreateTime(DateTool.getCurrTime());
        this.save(nmCoupon);
        return true;
    }

    @Override
    public NmCoupon getByName(String name,String createBy) {
        QueryWrapper<NmCoupon> queryWrapper = new QueryWrapper<>();
        String token = request.getHeader("Authorization");
        queryWrapper.eq("create_by", createBy);
        queryWrapper.eq("name", name);
        return this.getOne(queryWrapper);
    }

    @Override
    public List<NmCoupon> listByUserId(String userId) {
        QueryWrapper<NmCoupon> queryWrapper = new QueryWrapper<>();
        if (userId == null || userId.isEmpty()) {
            String token = request.getHeader("Authorization");
            userId = JwtUtil.validateToken(token);
        }
        queryWrapper.eq("create_by", userId);
        return this.list(queryWrapper);
    }

    @Override
    public List<NmCoupon> listByShopId(String shopId) {
        QueryWrapper<NmCoupon> queryWrapper = new QueryWrapper<>();
        log.warn("createBy1:"+shopId);
        if (shopId != null && !shopId.isEmpty()) {
            log.warn("createBy2:"+shopId);
            queryWrapper.eq("create_by", shopId);
            return this.list(queryWrapper);
        }
        log.warn("createBy3:"+shopId);
        return null;
    }

    @Override
    public Boolean update(NmCoupon nmCoupon) {
        this.updateById(nmCoupon);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public List<NmCoupon> listByName(String name) {
        QueryWrapper<NmCoupon> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            log.warn(name);
            queryWrapper.like("name", "%" + name + "%");
        }
        log.info(name);
        return this.list(queryWrapper);
    }
}
