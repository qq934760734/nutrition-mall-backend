package com.example.nutritionmall.example.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "CartGoodsShop实体",description = "购物车商品商店表")
public class CouponClaimsCoupon{
    /**
     * couponClaims
     */
    @ApiModelProperty(value = "UUID")
    private String id;
    @ApiModelProperty(value = "关联优惠券表")
    private String couponId;
    @ApiModelProperty(value = "关联用户表")
    private String userId;
    @ApiModelProperty(value = "关联订单表，如有")
    private String ordersId;
    @ApiModelProperty(value = "领取时间")
    private String claimDate;
    @ApiModelProperty(value = "0-未使用，1-已使用，2-已过期")
    private int claimStatus;
    /**
     * coupon
     */
    @ApiModelProperty(value = "可以选择“店铺id”，“商品id”，空的时候表示任何商品都可以使用这个优惠券")
    private String contentId;
    @ApiModelProperty(value = "可以选择“商品优惠券”“店铺优惠券”等")
    private String type;
    @ApiModelProperty(value = "折扣的方式（现金券，折扣券，满减券，礼品券）")
    private String methods;
    @ApiModelProperty(value = "优惠券名称")
    private String name;
    @ApiModelProperty(value = "满减条件（condition是关键字），x\n" +
            "当value_desc=“元”时，内容为\n" +
            "满 condition_value 元能优惠value，\n" +
            "当value_desc=\"折\"时，内容为\n" +
            "满condition_value 元，最多能优惠多少元")
    private String conditions;
    @ApiModelProperty(value = "达到多少元门槛可以使用")
    private BigDecimal conditionsValue;
    @ApiModelProperty(value = "开始时间")
    private String startTime;
    @ApiModelProperty(value = "结束时间")
    private String endTime;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "不可用时展示原因")
    private String reason;
    @ApiModelProperty(value = "折扣（9.88折）")
    private BigDecimal discount;
    @ApiModelProperty(value = "打折金额")
    private BigDecimal value;
    @ApiModelProperty(value = "打折时，最高折扣金额")
    private BigDecimal valueMax;
    @ApiModelProperty(value = "优惠多少钱，优惠券为折扣券时，此值为0.00")
    private BigDecimal valueDesc;
    @ApiModelProperty(value = "价格单位(元、折)")
    private String unitDesc;
    @ApiModelProperty(value = "使用次数")
    private int usageCount;
    @ApiModelProperty(value = "最大使用次数")
    private int maxUsage;
    @ApiModelProperty(value = "状态：0-有效，1-已过期，2-已用完")
    private int status;
}
