package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmAddress;

import java.util.List;

public interface NmAddressService extends IService<NmAddress>{
    Boolean add(NmAddress address);

    Boolean update(NmAddress address);

    void deleteByIds(String ids);

    List<NmAddress> list(String userId, Boolean isDefault);
}
