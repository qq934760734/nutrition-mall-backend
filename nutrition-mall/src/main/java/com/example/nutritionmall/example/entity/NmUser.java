package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("nm_user")
@ApiModel(value = "nm_user实体", description = "用户表")
public class NmUser extends BaseEntity {
    @ApiModelProperty(value = "用户名")
    private String name;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "昵称")
    private String nickname;
    @ApiModelProperty(value = "性别")
    private String sex;
    @ApiModelProperty(value = "年龄")
    private int age;
    @ApiModelProperty(value = "电话")
    private String telephone;
    @ApiModelProperty(value = "email")
    private String email;
    @ApiModelProperty(value = "图片")
    private String image;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "用户权限，0-用户，1-商家，2-管理员")
    private int permission;
    @ApiModelProperty(value = "用户权限是否可用，0-不可用 1-可用")
    private int status;
    @ApiModelProperty(value = "最后登录IP")
    private String loginIp;
    @ApiModelProperty(value = "最后登录时间 YYYY-MM-DD hh:mm:ss")
    private String loginDate;

    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;

}