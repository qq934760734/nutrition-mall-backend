package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/file")
@Api(tags = "FileController")
public class FileController {
    @Value("${prop.upload-image}")
    private String UPLOAD_IMAGE;

    @Value("${prop.upload-video}")
    private String UPLOAD_VIDEO;

    @PostMapping("/upload")
    public Result upload(@RequestParam(name = "file") MultipartFile file, HttpServletRequest request) {
        Result result = new Result();
        if (file == null) {
            result.fail("请选择要上传的图片");
            return result;
        }
        if (file.getSize() > 1024 * 1024 * 10) {
            result.fail("文件大小不能大于10M");
            return result;
        }
        //获取文件后缀
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1, file.getOriginalFilename().length());
        if (!"jpg,jpeg,gif,png".toUpperCase().contains(suffix.toUpperCase())) {
            result.fail("请选择jpg,jpeg,gif,png格式的图片");
            return result;
        }

        String savePath = UPLOAD_IMAGE;
        File savePathFile = new File(savePath);
        if (!savePathFile.exists()) {
            //若不存在该目录，则创建目录
            savePathFile.mkdir();
        }
        //通过UUID生成唯一文件名
        String filename = UUID.randomUUID().toString().replaceAll("-", "") + "." + suffix;
        try {
            //将文件保存指定目录
            file.transferTo(new File(savePath + filename));
        } catch (Exception e) {
            e.printStackTrace();
            result.fail("保存文件异常");
            return result;
        }

        //返回文件名称
//        String filePath = request.getScheme() + "://" + request.getServerName()
//                + ":" + request.getServerPort()  + filename;
        result.setData(filename);
        result.success("保存文件成功");
        return result;
//        return ResultUtil.success(ResultEnum.SUCCESS, filename, request);
    }

    @PostMapping("/uploadVideo")
    public Result uploadVideo(@RequestParam(name = "file") MultipartFile file, HttpServletRequest request) {
        Result result = new Result();
        if (file == null) {
            result.fail("请选择要上传的视频");
            return result;
        }
        if (file.getSize() > 1024 * 1024 * 10) {
            result.fail("文件大小不能大于10M");
            return result;
        }
        //获取文件后缀
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1, file.getOriginalFilename().length());
        if (!".mp4".toUpperCase().contains(suffix.toUpperCase())) {
            result.fail("请选择mp4格式的视频");
            return result;
        }
        String savePath = UPLOAD_VIDEO;
        File savePathFile = new File(savePath);
        if (!savePathFile.exists()) {
            //若不存在该目录，则创建目录
            savePathFile.mkdir();
        }
        //通过UUID生成唯一文件名
        String filename = UUID.randomUUID().toString().replaceAll("-", "") + "." + suffix;
        try {
            //将文件保存指定目录
            file.transferTo(new File(savePath + filename));
        } catch (Exception e) {
            e.printStackTrace();
            result.fail("保存文件异常");
            return result;
        }
        result.setData(filename);
        result.success("保存文件成功");
        return result;
    }


    /**
     * 删除文件
     *
     * @param fileName 这是图片的路径
     * @return
     */
    @PostMapping("/deleteFile")
    public Result deleteFile(String fileName) {
        Result result = new Result();
        String savePath = UPLOAD_IMAGE;
        File file = new File(savePath + fileName);
        //判断文件存不存在
        if (!file.exists()) {
            System.out.println("删除文件失败：" + fileName + "不存在！");
            result.fail("删除文件失败：" + fileName + "不存在！");
        } else {
            //判断这是不是一个文件，ps：有可能是文件夹
            if (file.isFile()) {
                if(file.delete()){

                }
                result.setData(file.delete());
                result.success("删除成功");
            } else {
                result.fail("不是一个文件/文件夹");
            }
        }
        return result;
    }
}
