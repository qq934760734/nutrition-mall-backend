package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.nutritionmall.example.entity.NmEvaluation;
import com.example.nutritionmall.example.vo.EvaluationOrdersGoodsShop;
import com.example.nutritionmall.example.vo.EvaluationUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NmEvaluationMapper extends BaseMapper<NmEvaluation> {
    @Select("SELECT e.*, g.id as `goodsId`, g.name as `goodsName`, g.category as `category`, g.tags, g.stock, " +
            "g.image as `goodsImage`, s.id as `shopId`, s.name as `shopName`,s.image as `shopImage`, " +
            "o.price, o.quantity, o.total_price , o.status as `status`, o.consignee_name as `consigneeName`, o.telephone as `telephone`, o.address as `address`, o.create_time as `createTime`, o.update_time as `updateTime`, g.description as `goodsDescription`" +
            "FROM nm_evaluation e, nm_goods g, nm_shop s , nm_orders o " +
            "WHERE e.user_id = '${userId}' AND e.orders_id = o.id AND o.goods_id = g.id AND g.shop_id = s.id " +
            "ORDER BY e.create_time DESC ")
    List<EvaluationOrdersGoodsShop> listByUserId(@Param("userId") String userId);

    @Select("SELECT e.*, u.nickname as `userNickname`, u.image as `userImage` " +
            "FROM nm_evaluation e, nm_user u, nm_orders o " +
            "WHERE e.user_id = u.id AND e.orders_id = o.id AND o.goods_id = '${goodsId}' " +
            "ORDER BY e.create_time ")
    Page<EvaluationUser> pageByGoodsId(@Param("page") Page<EvaluationUser> page, @Param("goodsId") String goodsId);
}
