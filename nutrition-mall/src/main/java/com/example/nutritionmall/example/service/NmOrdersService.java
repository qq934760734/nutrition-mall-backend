package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmOrders;
import com.example.nutritionmall.example.vo.OrdersGoodsShop;

import java.util.List;

public interface NmOrdersService extends IService<NmOrders> {
    Boolean add(List<NmOrders> ordersList);

    Boolean secKillAdd(NmOrders nmOrders);

    void deleteByIds(String ids);

    Boolean update(NmOrders nmOrders);

    List<OrdersGoodsShop> listMyOrders(String goodsName);

    List<OrdersGoodsShop> listByShopId(String shopId);

    List<OrdersGoodsShop> listByIdOrConsigneeNameOrStatus(String ordersId,String consigneeName,int status);

    List<String> listGoodsIdsByUserId(String userId);

    Boolean setStatus(String ids, int status, String reason);
}
