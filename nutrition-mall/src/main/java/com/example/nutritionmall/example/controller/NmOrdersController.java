package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.common.utils.ToolUtil;
import com.example.nutritionmall.common.utils.param.ParameterUtil;
import com.example.nutritionmall.common.utils.rabbitmq.MQSender;
import com.example.nutritionmall.common.utils.redis.RedisUtil;
import com.example.nutritionmall.example.entity.NmOrders;
import com.example.nutritionmall.example.service.NmGoodsService;
import com.example.nutritionmall.example.service.NmOrdersService;
import com.example.nutritionmall.example.service.impl.RedisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@RestController
@RequestMapping("/orders")
@Api(tags = "NmOrdersController", description = "订单项目controller，返回的baseUrl=http://localhost:8998/petStore/")
@Slf4j
public class NmOrdersController implements InitializingBean {

    @Autowired
    private NmOrdersService nmOrdersService;

    //批量添加订单项目
    @ApiOperation(value = "批量添加订单项目")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody List<NmOrders> ordersList) {
        Result result = new Result();
        nmOrdersService.add(ordersList);
        result.success("添加成功");
        result.setData(ordersList);
        return result;
    }

    //批量删除订单项目
    @ApiOperation(value = "批量删除订单项目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmOrdersService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    //修改订单项目
    @ApiOperation(value = "修改订单项目")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmOrders nmOrders) {
        Result result = new Result();
        nmOrdersService.update(nmOrders);
        result.success("修改成功");
        return result;
    }


    //查询当前用户的订单项目信息formInline
    @ApiOperation(value = "查询查询当前用户的订单项目信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsName", paramType = "query", value = "需要查询订单项目的商品名，可为空")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list(String goodsName) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmOrdersService.listMyOrders(goodsName));
        return result;
    }

    //查询指定id的订单项目信息
    @ApiOperation(value = "查询指定id的订单项目信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ordersId", required = true, paramType = "query", value = "需要查询的订单项目id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getById")
    public Result getById(String ordersId) {
        Result result = new Result();
        result.success("查询成功");
        result.setData(nmOrdersService.getById(ordersId));
        return result;
    }

    //查询指定商家id所关联的订单项目信息(若不指定id则查询当前商家所关联的订单)
    @ApiOperation(value = "查询指定商家id所关联的订单项目信息(若不指定id则查询当前商家所关联的订单)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ownerId", paramType = "query", value = "需要查询的订单所关联的商家id(若不指定则查询当前商家所关联的订单)"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/listByShopId")
    public Result listByShopId(String shopId) {
        Result result = new Result();
        result.setData(nmOrdersService.listByShopId(shopId));
        result.success("查询list成功");
        return result;
    }

    //批量修改订单项目状态
    @ApiOperation(value = "批量修改订单项目状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要修改的多个id，用逗号,隔开"),
            @ApiImplicitParam(name = "status", required = true, paramType = "query", value = "要修改成的状态"),
            @ApiImplicitParam(name = "reason", paramType = "query", value = "退单理由，可为空")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/setStatusByIds")
    public Result setStatusByIds(String ids, int status, String reason) {
        Result result = new Result();
        nmOrdersService.setStatus(ids, status, reason);
        result.success("修改成功");
        return result;
    }

    /**
     * 根据订单id或收货人姓名或订单的状态搜索
     *
     * @param ordersId,consigneeName,status
     * @return
     */
    @ApiOperation(value = "根据订单id或收货人姓名或订单的状态搜索")
    @RequestMapping(method = RequestMethod.POST, value = "/listByIdOrConsigneeNameOrStatus")
    public Result listByIdOrConsigneeNameOrStatus(String ordersId, String consigneeName, int status) {
        Result result = new Result();
        result.setData(nmOrdersService.listByIdOrConsigneeNameOrStatus(ordersId, consigneeName, status));
        result.success("查询list成功");
        return result;
    }


    /**
     * 秒杀
     */

    @Resource
    private MQSender mQSender;

    @Resource
    private RedisUtil redisUtil;

    @Resource
    private RedisTemplate<String, Object> secKillRedisTemplate;

    @Resource
    private NmGoodsService nmGoodsService;
    //内存标记，减少redis访问
    private HashMap<String, Boolean> localOverMap = new HashMap<String, Boolean>();

    @Resource
    private RedisService redisService;

    /**
     * 系统初始化时把商品库存加入到缓存中
     */
    @Override
    public void afterPropertiesSet() throws Exception {
//        //查询库存数量
//        List<Map<String, Object>> stockList = nmGoodsService.queryAllGoodStock();
//        System.out.println("系统初始化：" + stockList);
//        if (stockList.size() <= 0) {
//            return;
//        }
//        for (Map<String, Object> m : stockList) {
//            // 检查goodsId和goods_stock是否存在且不为null
//            String goodsId = (String) m.get("goodsId");
//            if (goodsId == null) {
//                // 处理goodsId为null的情况，例如记录日志或抛出异常
//                continue;
//            }
//            Number goodsStock = (Number) m.get("stock");
//            if (goodsStock == null) {
//                // 处理goods_stock为null的情况
//                continue;
//            }
//            // 将库存加载到redis中
//            secKillRedisTemplate.opsForValue().set(goodsId, goodsStock.toString());
////            redisUtil.getRedisTemplate().opsForValue().set(goodsId, goodsStock.toString());
//            // 添加内存标记
//            localOverMap.put(goodsId, false);
//        }
        //查询库存数量
        List<Map<String, Object>> stockList = nmGoodsService.queryAllGoodStock();
        System.out.println("系统初始化：" + stockList);
        if (stockList.size() <= 0) {
            return;
        }
        for (Map<String, Object> m : stockList) {
            //将库存加载到redis中
            secKillRedisTemplate.opsForValue().set(m.get("goodsId") + "", Integer.parseInt(m.get("stock").toString()));
            //添加内存标记
            localOverMap.put(m.get("goodsId").toString(), false);
        }
    }

    /**
     * 请求秒杀,redis+rabbitmq方式
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST, value = "/secKill")
    public Result secKillRabbitMQ(@RequestBody NmOrders nmOrders) throws Exception{
        Result result = new Result();
        String userId = nmOrders.getUserId();
        String goodsId = nmOrders.getGoodsId();
        long quantity = nmOrders.getQuantity();
        //1.根据需求   校验是否频繁请求
        //2.校验是否重复下单
        Map<String,Object> map = redisService.get("order_"+userId+"_"+goodsId,Map.class);
        if(map != null) {
            result.fail("重复下单");
            return result;
        }
        boolean over = localOverMap.get(goodsId);
        if (over) {
            result.fail("秒杀结束");
            return result;
        }
        //减去redis里对应id的库存数量
        System.out.println(quantity);
        long stock;
        if (quantity < 0) {
            throw new RuntimeException("递减因子必须大于0");
        } else {
            stock=secKillRedisTemplate.opsForValue().decrement(goodsId, quantity);
        }

        if (stock < 0) {
            localOverMap.put(goodsId, true);
            result.fail("库存不足");
            return result;
        }
        System.out.println("剩余库存：" + stock);
        //加入到队列中,返回0:排队中,客户端轮询或延迟几秒后查看结果
        Map<String, Object> msg = new HashMap<>();
        msg.put("user_id", userId);
        msg.put("goods_id", goodsId);
        msg.put("quantity", quantity);
        msg.put("shop_id", nmOrders.getShopId());
        msg.put("price", nmOrders.getPrice());
        msg.put("total_price", nmOrders.getTotalPrice());
        msg.put("consignee_name", nmOrders.getConsigneeName());
        msg.put("telephone", nmOrders.getTelephone());
        msg.put("address", nmOrders.getAddress());
        msg.put("status", nmOrders.getStatus());
        mQSender.send(msg);
        result.success("排队中");
        return result;
    }

    //查询秒杀结果(orderId：成功,-1：秒杀失败,0： 排队中)
    @RequestMapping(value = "/result", method = RequestMethod.GET)
    @ResponseBody
    public Result secKillResult(HttpServletRequest request) {
        Result result = new Result();
        ConcurrentHashMap<String, String> parameterMap = ParameterUtil.getParameterMap(request);
        String userid = parameterMap.get("userId").toString();
        String goodsId = parameterMap.get("goodsId").toString();

        String result1 = nmGoodsService.getMiaoshaResult(userid, goodsId);
        if (!result1.equals("0") && !result1.equals("-1")) {
            result.success("秒杀成功!");
            result.setData(result1);
        } else {
            result.success("秒杀失败!");
        }
        return result;
    }
}
