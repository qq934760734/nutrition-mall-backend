package com.example.nutritionmall.example;

import com.example.nutritionmall.common.base.BaseEntity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 用来存储 排序商品的方式
 */
@Data
public class ListByShopIdRequest extends BaseEntity {
    /**
     * 商店id
     */
    private String shopId;
    /**
     * 销量
     */
    private String sales;
    /**
     * 日销量
     */
    private String dailySales;
    /**
     * 月销量
     */
    private String monthlySales;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 创建时间
     */
    private String createTime;
}
