package com.example.nutritionmall.example.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "GoodsShopPage实体",description = "商品商店分页返回结果")
public class GoodsShopPage {
    // 信息记录
    private List<GoodsShop> records;
    // 总条数
    private Integer total;
    // 每页条数
    private Integer size;
    // 当前页数
    private Integer current;
    // 总页数
    private Integer pages;
}
