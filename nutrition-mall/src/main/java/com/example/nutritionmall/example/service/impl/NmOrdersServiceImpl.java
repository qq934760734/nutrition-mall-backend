package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.JwtUtil;
import com.example.nutritionmall.common.utils.rabbitmq.SendMessageUtil;
import com.example.nutritionmall.example.entity.NmOrders;
import com.example.nutritionmall.example.mapper.NmOrdersMapper;
import com.example.nutritionmall.example.service.NmGoodsService;
import com.example.nutritionmall.example.service.NmOrdersService;
import com.example.nutritionmall.example.vo.OrdersGoodsShop;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class NmOrdersServiceImpl extends ServiceImpl<NmOrdersMapper, NmOrders> implements NmOrdersService {

    @Resource
    private HttpServletRequest request;
    @Resource
    private SendMessageUtil sendMessageUtil;
    @Resource
    private NmGoodsService nmGoodsService;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private RedisService redisService;

    private static final String DAILY_SALES = "dailySales";

    @Override
    public Boolean add(List<NmOrders> ordersList) {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        Map<String, Integer> salesMap = new HashMap<>();
        for (NmOrders order : ordersList) {
            order.setUserId(userId);
            order.setCreateTime(DateTool.getCurrTime());
            this.save(order);
            // 将goodsId和quantity存入map以用于redis增加销量操作
            //salesMap.put(order.getGoodsId(), order.getQuantity());
            // 加入消息队列
            Map<String, Object> map = new HashMap<>();
            map.put("ordersId", order.getId());
            sendMessageUtil.sendDirectMessage(map);
        }
        // 使用管道批量执行redis操作
//        this.increaseDailySales(salesMap);
        return true;
    }

    /**
     * 下订单,增加缓存
     * 失败，回滚
     * @param nmOrders
     * @return
     */
    @Override
    @Transactional
    public Boolean secKillAdd(NmOrders nmOrders){
        try {
            log.warn(nmOrders.getGoodsId());
            this.save(nmOrders);
            //将生成的订单放入缓存
            redisService.set("order_"+nmOrders.getUserId()+"_"+nmOrders.getGoodsId(), nmOrders);
            return true;
        } catch (Exception e) {
            //出现异常手动回滚
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            redisService.incr("goods"+nmOrders.getGoodsId());
            return false;
        }
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmOrders nmOrders) {
        this.updateById(nmOrders);
        return true;
    }

    @Override
    public List<OrdersGoodsShop> listMyOrders(String goodsName) {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        return baseMapper.listByGoodsName(userId, goodsName);
    }

    @Override
    public List<OrdersGoodsShop> listByShopId(String shopId) {
        String id = new String();
        if (shopId == null || shopId.isEmpty()) {
            // 若传入的id为空，则查询当前商家的订单项目，否则查询指定商家的订单项目
            String token = request.getHeader("Authorization");
            id = JwtUtil.validateToken(token);
        } else {
            id = shopId;
        }
        return baseMapper.listByShopId(id);
    }

    // 根据用户id查询该用户购买过的商品id(去重)
    @Override
    public List<String> listGoodsIdsByUserId(String userId) {
        QueryWrapper<NmOrders> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("goods_id");
        queryWrapper.eq("user_id", userId);
        List<NmOrders> ordersList = this.list(queryWrapper);
        // 去重，去除goodsIds内的重复项
        Set<String> goodsIds = new HashSet<>();
        for (NmOrders order : ordersList) {
            goodsIds.add(order.getGoodsId());
        }
        return new ArrayList<>(goodsIds);
    }

    @Override
    public Boolean setStatus(String ids, int status, String reason) {
        log.info(ids);
        String[] aryIds = ids.split(",");
        Map<String, Integer> stockMap = new HashMap<>();
        for (String id : aryIds) {
            NmOrders nmOrders = this.getById(id);
            if (nmOrders != null) {
                if (status == -2 || status == -1 || status == -3) {
                    //记录申请退款前的订单状态
                    nmOrders.setPreStatus(nmOrders.getStatus());
                }
                nmOrders.setStatus(status);
                nmOrders.setReason(reason);
                this.updateById(nmOrders);
                //订单状态，0-待支付，1-待发货，2-待收货，3-待评价，4-售后/退款，5-已完成，
                // -1-已取消，-2-申请退款，-3-商家直接退款，-4-退款成功

                // 1、未支付-》已取消 2、申请退款-》退款成功 3.商家直接退款
                // 若目标状态为取消订单、退款申请通过、商家直接退款，则更新库存
                if (status == -1 || status == -3 || status == -4) {
                    // 若map内已有该商品id，则累加库存
                    Integer quantity = stockMap.get(nmOrders.getGoodsId());
                    if (quantity != null) {
                        quantity += nmOrders.getQuantity();
                    } else {
                        quantity = nmOrders.getQuantity();
                    }
                    stockMap.put(nmOrders.getGoodsId(), quantity);
                }
            }
        }
        nmGoodsService.updateStockByMap(stockMap, "add");
        return true;
    }

    // 增加日销量
    public Boolean increaseDailySales(Map<String, Integer> salesMap) {
        // 获取当前时间到后31天0点的毫秒数
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nextMidnight = LocalDateTime.of(now.plusDays(31).toLocalDate(), LocalTime.MIDNIGHT);
        long timeout = Duration.between(now, nextMidnight).toMillis();
        // 格式化当前日期
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = now.format(formatter);
        // 使用管道批量传输redis命令以减少网络io
        redisTemplate.executePipelined(new SessionCallback<Object>() {
            @Override
            public <K, V> Object execute(RedisOperations<K, V> operations) throws DataAccessException {
                for (Map.Entry<String, Integer> entry : salesMap.entrySet()) {
                    String goodsId = entry.getKey();
                    Integer quantity = entry.getValue();
                    String key = DAILY_SALES + ":" + formattedDate + ":" + goodsId;
                    // 增加库存并设置过期时间（若key不存在会自动创建）
                    operations.opsForValue().increment((K) key, quantity.longValue());
                    operations.expire((K) key, timeout, TimeUnit.MILLISECONDS);
                }
                return null;
            }
        });
        return true;
    }

    @Override
    public List<OrdersGoodsShop> listByIdOrConsigneeNameOrStatus(String ordersId, String consigneeName, int status) {
        return baseMapper.listByIdOrConsigneeNameOrStatus(ordersId, consigneeName, status);
    }
}

