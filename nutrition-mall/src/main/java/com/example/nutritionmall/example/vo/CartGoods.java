package com.example.nutritionmall.example.vo;

import com.example.nutritionmall.example.entity.NmCart;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "CartGoods实体",description = "购物车商品表")
public class CartGoods extends NmCart {
    @ApiModelProperty(value = "商品名")
    private String name;
    @ApiModelProperty(value = "单价")
    private String price;
    @ApiModelProperty(value = "图片路径")
    private String image;
    @ApiModelProperty(value = "商品类别")
    private String category;
    @ApiModelProperty(value = "商品标签")
    private String tags;
}
