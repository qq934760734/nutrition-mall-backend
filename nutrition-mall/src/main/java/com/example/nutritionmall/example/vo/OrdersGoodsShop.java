package com.example.nutritionmall.example.vo;

import com.example.nutritionmall.example.entity.NmOrders;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "OrdersGoodsShop实体",description = "订单项目商品商店表")
public class OrdersGoodsShop extends NmOrders {
    @ApiModelProperty(value = "商店名")
    private String goodsName;
    @ApiModelProperty(value = "商店分类")
    private String category;
    @ApiModelProperty(value = "分类标签")
    private String tags;
    @ApiModelProperty(value = "库存")
    private String stock;
    @ApiModelProperty(value = "商品图片路径")
    private String goodsImage;
    @ApiModelProperty(value = "店铺id")
    private String shopId;
    @ApiModelProperty(value = "商品描述")
    private String goodsDescription;
    @ApiModelProperty(value = "店铺名称")
    private String shopName;
    @ApiModelProperty(value = "店铺图片路径")
    private String shopImage;
    @ApiModelProperty(value = "店铺地址")
    private String shopLocation;
    @ApiModelProperty(value = "店铺介绍")
    private String shopDescription;
}
