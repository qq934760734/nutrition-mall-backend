package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmGoods;
import com.example.nutritionmall.example.entity.NmOrders;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SecKillMapper extends BaseMapper<NmOrders> {

    Map<String, Object> queryGoodStockById(@Param("m") Map<String, Object> m);

    List<Map<String, Object>> queryAllGoodStock();

    List<Map<String, Object>> queryOrderByUserIdAndCoodsId(@Param("m") Map<String, Object> m);

    Integer updateGoodStock(Map<String, Object> m);

    Integer insertOrder(Map<String, Object> m);


}
