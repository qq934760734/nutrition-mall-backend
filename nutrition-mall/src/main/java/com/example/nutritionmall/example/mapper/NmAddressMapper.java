package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmAddress;

public interface NmAddressMapper extends BaseMapper<NmAddress> {
}
