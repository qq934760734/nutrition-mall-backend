package com.example.nutritionmall.example.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.example.entity.NmVideo;
import com.example.nutritionmall.example.mapper.NmVideoMapper;
import com.example.nutritionmall.example.service.NmVideoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NmVideoServiceImpl extends ServiceImpl<NmVideoMapper, NmVideo> implements NmVideoService {

    @Override
    public Boolean add(NmVideo nmVideo) {
        nmVideo.setCreateTime(DateTool.getCurrTime());
        this.save(nmVideo);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmVideo nmVideo) {
        this.updateById(nmVideo);
        return true;
    }


    @Override
    public List<NmVideo> listByShopId(String goodsId, String name, Double priceMin, Double priceMax) {
        QueryWrapper<NmVideo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("goods_id", goodsId);
        if (name != null) {
            queryWrapper.like("name", name);
        }
        if (priceMin != null) {
            queryWrapper.ge("price", priceMin);
        }
        if (priceMax != null) {
            queryWrapper.lt("price", priceMax);
        }
        queryWrapper.orderByDesc("create_time");
        return this.list(queryWrapper);
    }

    @Override
    public List<NmVideo> list(String name, Double priceMin, Double priceMax) {
        QueryWrapper<NmVideo> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", name);
        }
        if (priceMin != null) {
            queryWrapper.ge("price", priceMin);
        }
        if (priceMax != null) {
            queryWrapper.lt("price", priceMax);
        }
        queryWrapper.orderByDesc("create_time");
        return this.list(queryWrapper);
    }

    @Override
    public List<NmVideo> getVideoByGoodsId(String goodsId) {
        QueryWrapper<NmVideo> queryWrapper = new QueryWrapper<>();
        if (goodsId != null) {
            queryWrapper.like("goods_id", goodsId);
        }
        return this.list(queryWrapper);
    }

}
