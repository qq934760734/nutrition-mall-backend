package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmGoodsCategory;
import com.example.nutritionmall.example.service.NmGoodsCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/goodsCategory")
@Api(tags = "NmGoodsCategoryController", description = "类别controller，返回的baseUrl=http://localhost:8998/nutritionmall/")
public class NmGoodsCategoryController {

    @Autowired
    private NmGoodsCategoryService nmGoodsCategoryService;

    //添加类别
    @ApiOperation(value = "添加类别")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmGoodsCategory nmGoodsCategory) {
        Result result = new Result();
        NmGoodsCategory userNameExit = nmGoodsCategoryService.isAlive(nmGoodsCategory.getName());
        if (userNameExit != null) {
            result.fail("用户名:" + nmGoodsCategory.getName() + " 已存在");
        } else {
            nmGoodsCategoryService.add(nmGoodsCategory);
            result.success("添加成功");
            result.setData(nmGoodsCategory);
        }
        return result;
    }

    //批量删除类别-deleteByIds
    @ApiOperation(value = "批量删除类别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmGoodsCategoryService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    //修改类别
    @ApiOperation(value = "修改类别")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmGoodsCategory nmGoodsCategory) {
        Result result = new Result();
        nmGoodsCategoryService.update(nmGoodsCategory);
        result.success("修改成功");
        return result;
    }

    //查询所有类别信息（可指定name）
    @ApiOperation(value = "查询所有类别信息（可指定name）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", paramType = "query", value = "类别关键字，可以为空"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list(String name) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsCategoryService.list(name));
        return result;
    }

    //查-getById
    @ApiOperation(value = "通过id查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true,paramType = "query",value = "需要查询的id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getById")
    public Result getById(String id) {
        Result result = new Result();
        NmGoodsCategory nmGoodsCategory = nmGoodsCategoryService.getById(id);
        if (nmGoodsCategory != null){
            result.success("获取成功");
            result.setData(nmGoodsCategory);
        }else{
            result.fail("获取失败");
        }
        return result;
    }

    //根据用户ID查询所有类别信息
    @ApiOperation(value = "查询所有类别信息")
    @RequestMapping(method = RequestMethod.POST, value = "/listByUserId")
    public Result listByUserId(String createBy) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsCategoryService.listByUserId(createBy));
        return result;
    }

    /**
     *
     * @param name
     * @return
     */
    //模糊查找%name%类别信息
    @ApiOperation(value = "查询所有类别信息（可指定name）")
    @RequestMapping(method = RequestMethod.POST, value = "/listByName")
    public Result listByName(String name) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmGoodsCategoryService.listByName(name));
        return result;
    }
}
