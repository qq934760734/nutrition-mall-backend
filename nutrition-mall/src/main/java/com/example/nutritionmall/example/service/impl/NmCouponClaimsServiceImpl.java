package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.JwtUtil;
import com.example.nutritionmall.example.entity.NmCouponClaims;
import com.example.nutritionmall.example.mapper.NmCouponClaimsMapper;
import com.example.nutritionmall.example.service.NmCouponClaimsService;
import com.example.nutritionmall.example.vo.CouponClaimsCoupon;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Service
public class NmCouponClaimsServiceImpl extends ServiceImpl<NmCouponClaimsMapper, NmCouponClaims> implements NmCouponClaimsService {

    @Autowired
    private HttpServletRequest request;

    @Override
    public Boolean add(NmCouponClaims nmCouponClaims) {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        nmCouponClaims.setUserId(userId);
        this.save(nmCouponClaims);
        return true;
    }

    @Override
    public NmCouponClaims isExit(String userId, String couponId) {
        QueryWrapper<NmCouponClaims> queryWrapper = new QueryWrapper<>();
        if (userId == null || userId.isEmpty()) {
            String token = request.getHeader("Authorization");
            userId = JwtUtil.validateToken(token);
        }
        queryWrapper.eq("coupon_id", couponId);
        queryWrapper.eq("user_id", userId);
        return this.getOne(queryWrapper);
    }

    //    @Override
//    public List<NmCouponClaims> listByUserId(String userId) {
//        QueryWrapper<NmCouponClaims> queryWrapper = new QueryWrapper<>();
//        if (userId == null || userId.isEmpty()) {
//            String token = request.getHeader("Authorization");
//            userId = JwtUtil.validateToken(token);
//        }
//        queryWrapper.eq("user_id", userId);
//        return this.list(queryWrapper);
//    }
    @Override
    public List<CouponClaimsCoupon> list(String userId) {
        String token = request.getHeader("Authorization");
        userId = JwtUtil.validateToken(token);
        return baseMapper.listByUserId(userId);
    }
}
