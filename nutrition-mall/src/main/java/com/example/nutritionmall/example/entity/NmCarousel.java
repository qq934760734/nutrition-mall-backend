package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("nm_carousel")
@ApiModel(value = "nm_carousel", description = "轮播图表，只有管理员能存放")
public class NmCarousel extends BaseEntity {
    @ApiModelProperty(value = "图片地址")
    private String url;
    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}