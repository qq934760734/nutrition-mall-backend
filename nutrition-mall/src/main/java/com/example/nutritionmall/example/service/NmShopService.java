package com.example.nutritionmall.example.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmShop;

import java.util.List;

public interface NmShopService extends IService<NmShop> {
    Boolean add(NmShop nmShop);

    void deleteByIds(String ids);

    Boolean update(NmShop nmShop);

    List<NmShop> list(String name);

    Page<NmShop> page(String adminId, String name, Integer pageNum, Integer pageSize);

    List<NmShop>  listByOwnerId(String ownerId, String name);
}
