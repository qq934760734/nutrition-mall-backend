package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("nm_goods")
@ApiModel(value = "nm_goods实体", description = "商品表")
public class NmGoods extends BaseEntity {
    @ApiModelProperty(value = "商品名称")
    private String name;
    @ApiModelProperty(value = "商品单价")
    private BigDecimal price;
    @ApiModelProperty(value = "商品种类")
    private String category;
    @ApiModelProperty(value = "分类标签")
    private String tags;
    @ApiModelProperty(value = "库存")
    private int stock;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "主图路径")
    private String image;
    @ApiModelProperty(value = "主视频路径")
    private String video;
    @ApiModelProperty(value = "商店id")
    private String shopId;
    @ApiModelProperty(value = "是否可用 0-不可用，1-可用")
    private int status;
    @ApiModelProperty(value = "是否是秒杀商品 0-不是 1-是")
    private int secKill;

    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}