package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmCarousel;
import com.example.nutritionmall.example.service.NmCarouselService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/carousel")
@Api(tags = "NmCarouselController", description = "轮播图图片controller，返回的baseUrl=http://localhost:8998/NutritionMall/")
public class NmCarouselController {
    @Resource
    private NmCarouselService nmCarouselService;

    /**
     * 新增轮播图
     * @param nmCarousel
     * @return
     */
    @ApiOperation(value = "添加轮播图")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmCarousel nmCarousel) {
        Result result = new Result();
        nmCarouselService.add(nmCarousel);
        result.success("添加成功");
        result.setData(nmCarousel);
        return result;
    }

    /**
     * 删除轮播图
     * @param ids
     * @return
     */
    @ApiOperation(value = "批量删除轮播图")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmCarouselService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    /**
     * 查询所有轮播图信息
     * @return
     */
    @ApiOperation(value = "查询所有轮播图信息")
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list() {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmCarouselService.list());
        return result;
    }

    /**
     * 修改轮播图
     * @param nmCarousel
     * @return
     */
    @ApiOperation(value = "修改轮播图")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmCarousel nmCarousel) {
        Result result = new Result();
        nmCarouselService.update(nmCarousel);
        result.success("修改成功");
        return result;
    }
}
