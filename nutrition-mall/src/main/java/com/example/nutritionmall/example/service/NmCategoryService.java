package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmCategory;
import com.example.nutritionmall.example.entity.NmUser;

import java.util.List;

public interface NmCategoryService extends IService<NmCategory> {
    Boolean add(NmCategory NmCategory);

    void deleteByIds(String ids);

    Boolean update(NmCategory NmCategory);

    List<NmCategory> list(String name);

    List<NmCategory> listByUserId(String createBy);

    NmCategory isAlive(String name,String userName);

    List<NmCategory> listByName(String name);
}
