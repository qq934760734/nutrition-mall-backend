package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("nm_category")
@ApiModel(value = "nm_category实体", description = "分类表")
public class NmCategory extends BaseEntity {
    @ApiModelProperty(value = "类名")
    private String name;
    @ApiModelProperty(value = "用户名")
    private String userName;
    @ApiModelProperty(value = "权限是否可用，0-不可用 1-可用")
    private int status;

    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}