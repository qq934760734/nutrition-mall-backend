package com.example.nutritionmall.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmCarousel;

import java.util.List;

public interface NmCarouselService extends IService<NmCarousel> {

    Boolean add(NmCarousel nmCarousel);

    void deleteByIds(String ids);

    List<NmCarousel> list();

    Boolean update(NmCarousel nmCarousel);
}
