package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmCart;
import com.example.nutritionmall.example.service.NmCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cart")
@Api(tags = "NmCartController", description = "购物车controller，返回的baseUrl=http://localhost:8998/petStore/")
public class NmCartController {

    @Autowired
    private NmCartService nmCartService;

    //添加购物车
    @ApiOperation(value = "添加购物车")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmCart nmCart) {
        Result result = new Result();
        nmCartService.add(nmCart);
        if(nmCart.getQuantity()==1){
            result.success("添加成功");
        }else{
            result.success("已在购物车，商品数量已增加");
        }
        result.setData(nmCart);
        return result;
    }

    //批量删除购物车-deleteByIds
    @ApiOperation(value = "批量删除购物车")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        Boolean flag = false;
        flag = nmCartService.deleteByIds(ids);
        if(flag) {
            result.success("删除成功");
        }else {
            result.fail("删除失败");
        }
        return result;
    }

    //修改购物车
    @ApiOperation(value = "修改购物车")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmCart nmCart) {
        Result result = new Result();
        nmCartService.update(nmCart);
        result.success("修改成功");
        return result;
    }

    //查询用户所有购物车信息（可指定商品名）
    @ApiOperation(value = "查询用户所有购物车信息（可指定商品名）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsName", paramType = "query", value = "书名关键字，可以为空")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list(String goodsName) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmCartService.listByGoodsName(goodsName));
        return result;
    }

    //查询用户所有购物车信息，包括商品信息和商店信息
    @ApiOperation(value = "查询用户所有购物车信息，包括商品信息和商店信息")
    @RequestMapping(method = RequestMethod.POST, value = "/listAllInfo")
    public Result listAllInfo() {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmCartService.listFullInfo());
        return result;
    }

    //分页查询（可指定商品名）
    @ApiOperation(value = "分页返回购物车信息（可指定商品名）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",  required = true, paramType = "userId", value = "用户id"),
            @ApiImplicitParam(name = "name", paramType = "query", value = "书名关键字，可以为空"),
            @ApiImplicitParam(name = "pageNum", required = true, paramType = "query", value = "当前页码"),
            @ApiImplicitParam(name = "pageSize", required = true, paramType = "query", value = "每页显示条数"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/page")
    public Result page(String userId, String goodsName, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        result.success("查询page成功");
        result.setData(nmCartService.page(goodsName, pageNum, pageSize));
        return result;
    }

    //查-getById
    @ApiOperation(value = "通过id查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true,paramType = "query",value = "需要查询的id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getById")
    public Result getById(String id) {
        Result result = new Result();
        NmCart nmCart = nmCartService.getById(id);
        if (nmCart != null){
            result.success("获取成功");
            result.setData(nmCart);
        }else{
            result.fail("获取失败");
        }
        return result;
    }

    //通过goodsId查询
    @ApiOperation(value = "通过goodsId查询")
    @RequestMapping(method = RequestMethod.POST, value = "/getByGoodsId")
    public Result getByGoodsId(String goodsId) {
        Result result = new Result();
        NmCart nmCart = nmCartService.getByGoodsId(goodsId);
        result.success("查询成功");
        result.setData(nmCart);
        return result;
    }

}