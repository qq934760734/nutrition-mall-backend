package com.example.nutritionmall.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.nutritionmall.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("nm_images")
@ApiModel(value = "nm_images实体", description = "图片表，用于存放商品主图以外的图片")
public class NmImages extends BaseEntity {
    @ApiModelProperty(value = "图片地址")
    private String image;
    @ApiModelProperty(value = "商品id")
    private String goodsId;
    @ApiModelProperty(value = "商店id")
    private String shopId;
    @ApiModelProperty(value = "创建者")
    private String createBy;
    @ApiModelProperty(value = "创建时间 YYYY-MM-DD hh:mm:ss")
    private String createTime;
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    @ApiModelProperty(value = "更新时间 YYYY-MM-DD hh:mm:ss")
    private String updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}
