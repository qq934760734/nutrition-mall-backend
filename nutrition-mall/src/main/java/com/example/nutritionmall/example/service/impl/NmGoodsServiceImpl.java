package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.example.ListByShopIdRequest;
import com.example.nutritionmall.example.vo.GoodsShop;
import com.example.nutritionmall.example.entity.NmGoods;
import com.example.nutritionmall.example.mapper.NmGoodsMapper;
import com.example.nutritionmall.example.service.NmGoodsService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Slf4j
@Service
public class NmGoodsServiceImpl extends ServiceImpl<NmGoodsMapper, NmGoods> implements NmGoodsService {

    @Resource
    private RedisTemplate redisTemplate;
    private static final String GOODS_VIEWS = "goodsViews";
    private static final String DAILY_SALES = "dailySales";

    @Override
    public Boolean add(NmGoods nmGoods) {
        nmGoods.setCreateTime(DateTool.getCurrTime());
        this.save(nmGoods);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmGoods nmGoods) {
        this.updateById(nmGoods);
        return true;
    }

    @Override
    public List<GoodsShop> list(String id) {
        QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
        if (id != null) {
            queryWrapper.like("id", id);
        }
        queryWrapper.orderByDesc("create_time");
        List<NmGoods> goodsList = this.list(queryWrapper);
        List<GoodsShop> goodsShopList = new ArrayList<GoodsShop>();
        for (NmGoods item : goodsList) {
            goodsShopList.add(baseMapper.listByGoodsId(item.getId()));
        }
        return goodsShopList;
    }

    @Override
    public List<GoodsShop> listBySecKill() {
        QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sec_kill", 1);
        queryWrapper.orderByDesc("create_time");
        List<NmGoods> goodsList = this.list(queryWrapper);
        List<GoodsShop> goodsShopList = new ArrayList<GoodsShop>();
        for (NmGoods item : goodsList) {
            goodsShopList.add(baseMapper.listByGoodsId(item.getId()));
        }
        return goodsShopList;
    }
    @Override
    public List<GoodsShop> listByIds(String ids) {
        List<GoodsShop> list = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            list.add(baseMapper.listByGoodsId(id));
        }
        return list;
    }


    @Override
    public List<GoodsShop> listByCategory(String category) {
        QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
        if (category != null) {
            queryWrapper.like("category", category);
        }
        queryWrapper.orderByDesc("create_time");
        List<NmGoods> goodsList = this.list(queryWrapper);
        List<GoodsShop> goodsShopList = new ArrayList<GoodsShop>();
        for (NmGoods item : goodsList) {
            goodsShopList.add(baseMapper.listByGoodsId(item.getId()));
        }
        return goodsShopList;
    }

    @Override
    public List<GoodsShop> listByCategoryAndName(String shopId, String category, String name) {
        QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
        if (shopId != null) {
            queryWrapper.eq("shop_id", shopId);
        }
        if (category != null) {
            queryWrapper.like("category", category);
        }
        if (name != null) {
            queryWrapper.like("name", name);
        }
        queryWrapper.orderByDesc("create_time");
        List<NmGoods> goodsList = this.list(queryWrapper);
        List<GoodsShop> goodsShopList = new ArrayList<GoodsShop>();
        for (NmGoods item : goodsList) {
            goodsShopList.add(baseMapper.listByGoodsId(item.getId()));
        }
        return goodsShopList;
    }

    //    @Override
//    public List<NmGoods> listByShopId(String shopId) {
//        QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("shop_id", shopId);
//        queryWrapper.orderByDesc("create_time");
//        return this.list(queryWrapper);
//    }
    @Override
    public List<NmGoods> listByShopId(ListByShopIdRequest request) {
        QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
        log.info(request.getShopId());
        queryWrapper.eq("shop_id", request.getShopId());

        BigDecimal bd1 = new BigDecimal("102");
        BigDecimal bd2 = new BigDecimal("201");
        if (request.getPrice() != null) {
            int result1 = request.getPrice().compareTo(bd1);
            int result2 = request.getPrice().compareTo(bd2);

            if (result1 == 0) {
                //按价格升序
                queryWrapper.orderByAsc("price");
            }
            if (result2 == 0) {
                //按价格降序
                queryWrapper.orderByDesc("price");
            }
        }
        if (request.getCreateTime() != null) {
            if (request.getCreateTime().equals("createTime")) {
                //按时间升序
                queryWrapper.orderByAsc("create_time");
            }
            if (request.getCreateTime().equals("createTimeDesc")) {
                //按时间降序
                queryWrapper.orderByDesc("create_time");
            }
        }

        return this.list(queryWrapper);
    }

    @Override
    public NmGoods getByName(String name) {
        QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name);
        return this.getOne(queryWrapper);
    }

    @Override
    public List<NmGoods> goodsOrderByPriceDesc(String name) {
        QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", name);
        }
        queryWrapper.orderByDesc("price");
        List<NmGoods> goodsList = this.list(queryWrapper);

        return goodsList;
    }

    @Override
    public Map<String, List<NmGoods>> getGoodsByShopIds(String shopIds) {

        Map<String, List<NmGoods>> shopGoods = new HashMap<String, List<NmGoods>>();
        String[] aryIds = shopIds.split(",");
        for (String id : aryIds) {
            QueryWrapper<NmGoods> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("shop_id", id);
            List<NmGoods> goodsList = this.list(queryWrapper);
            shopGoods.put(id, goodsList);
        }
        return shopGoods;
    }

    /**
     * 判断库存是否足够，map内的key为要更新的商品id，value为要减少的库承数
     *
     * @param stockMap
     * @return
     */
    @Override
    public List<String> checkStock(Map<String, Integer> stockMap) {
        ArrayList<String> names = new ArrayList<>();
        for (Map.Entry entry : stockMap.entrySet()) {
            String goodsId = (String) entry.getKey();
            Integer quantity = (Integer) entry.getValue();
            NmGoods nmGoods = this.getById(goodsId);
            if (nmGoods.getStock() <= 0 || nmGoods.getStock() < quantity) {
                names.add(nmGoods.getName());
            }
        }
        return names;
    }

    /**
     * 更新库存，map内的key为要更新的商品id，value为要增减库存的数量
     * 若库充足，则扣减相应库存并返回null
     * 若存在某件商品库存不足，则记录下要库存不足的商品名，遍历完成后回滚所有操作并返回库存不足的商品名
     * 开启事务管理，库存不足或商品不存在时回滚操作
     *
     * @param stockMap
     * @param operator
     * @return
     */

    @Override
    @Transactional
    public List<String> updateStockByMap(Map<String, Integer> stockMap, String operator) {
        Boolean needRollback = false;
        ArrayList<String> ids = new ArrayList<String>();
        ArrayList<String> names = new ArrayList<String>();
        // 遍历map
        for (Map.Entry entry : stockMap.entrySet()) {
            String id = (String) entry.getKey();
            Integer quantity = (Integer) entry.getValue();
            NmGoods nmGoods = this.getById(id);
            // 减库存
            if (operator.equals("reduce")) {
                if (nmGoods != null && nmGoods.getStock() - quantity >= 0) {
                    // 若存在某件商品库存不足，则后续不在对数据库进行操作，只记录后续库存不足的商品名，以减少性能浪费
                    if (needRollback) {
                        continue;
                    }
                    // 扣减库存
                    nmGoods.setStock(nmGoods.getStock() - quantity);
                    this.updateById(nmGoods);
                } else {
                    needRollback = true;    // 回滚标记
                    // ids、names用于日志输出
                    ids.add(id);
                    if (nmGoods != null) {
                        names.add(nmGoods.getName());
                    }
                }
                // 加库存
            } else if (operator.equals("add")) {
                // 加库存
                if (nmGoods != null) {
                    nmGoods.setStock(nmGoods.getStock() + quantity);
                    this.updateById(nmGoods);
                } else {
                    ids.add(id);
                }

            } else {
                needRollback = true; // 回滚标记
                log.warn(id + "：操作不存在");
            }
        }
        // 回滚并返回库存不足的商品名
        if (needRollback) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.warn("存在商品库存不足或商品不存在，商品ids：" + ids);
            log.warn("库存不足的商品名如下：" + names);
        }
        return names;
    }

    @Override
    public List<NmGoods> listByIdOrGoodsNameOrCategory(String goodsId, String goodsName, String category) {
        return baseMapper.listByIdOrGoodsNameOrCategory(goodsId, goodsName, category);
    }


    /**
     * 秒杀
     */

    @Autowired
    private RedisService redisService;

    /**
     * 增加商品浏览量
     *
     * @param goodsId
     * @return
     */
    @Override
    public Integer increaseViews(String goodsId) {
        // 判断goodsViews是否存在，若不存在则创建一个
        Boolean exists = redisTemplate.hasKey(GOODS_VIEWS);
        if (exists) {
            redisTemplate.opsForHash().increment(GOODS_VIEWS, goodsId, 1);
        } else {
            redisTemplate.opsForHash().put(GOODS_VIEWS, goodsId, 1);
        }
        return (Integer) redisTemplate.opsForHash().get(GOODS_VIEWS, goodsId);
    }

    //查询全部商品库存数量
    @Override
    public List<Map<String, Object>> queryAllGoodStock() {
        return baseMapper.queryAllGoodStock();
    }


    //通过商品ID查询库存数量
    @Override
    public Map<String, Object> queryGoodStockById(Map<String, Object> m) {
        return baseMapper.queryGoodStockById(m);
    }


    //根据用户ID和商品ID查询是否下过单
    @Override
    public List<Map<String, Object>> queryOrderByUserIdAndCoodsId(Map<String, Object> m) {
        return baseMapper.queryOrderByUserIdAndCoodsId(m);
    }


    //获取秒杀结果
    @SuppressWarnings("unchecked")
    public String getMiaoshaResult(String userId, String goodsId) {

        Map<String, Object> orderMap = redisService.get("order" + userId + "_" + goodsId, Map.class);
        if (orderMap != null) {//秒杀成功
            return orderMap.get("id").toString();
        } else {
            boolean isOver = getGoodsOver(goodsId);
            if (isOver) {
                return "-1";
            } else {
                return "0";
            }
        }
    }

    //查询是否卖完了
    private boolean getGoodsOver(String goodsId) {
        return redisService.exists("goodsover" + goodsId);
    }

}
