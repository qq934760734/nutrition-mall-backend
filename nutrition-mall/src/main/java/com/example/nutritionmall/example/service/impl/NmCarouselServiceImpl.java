package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.example.entity.NmCarousel;
import com.example.nutritionmall.example.mapper.NmCarouselMapper;
import com.example.nutritionmall.example.service.NmCarouselService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class NmCarouselServiceImpl extends ServiceImpl<NmCarouselMapper, NmCarousel> implements NmCarouselService {
    @Override
    public Boolean add(NmCarousel nmCarousel) {
        this.save(nmCarousel);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public List<NmCarousel> list() {
        QueryWrapper<NmCarousel> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        return this.list(queryWrapper);
    }

    @Override
    public Boolean update(NmCarousel nmCarousel) {
        this.updateById(nmCarousel);
        return true;
    }
    
}
