package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmOrders;
import com.example.nutritionmall.example.vo.OrdersGoodsShop;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NmOrdersMapper extends BaseMapper<NmOrders> {
    @Select("SELECT o.*, g.name as `name`, g.category as `category`, g.tags, g.stock, g.image as `goodsImage`, g.shop_id, s.name as `shopName`,s.image as `shopImage` " +
            "FROM nm_orders o, nm_goods g, nm_shop s " +     //！！行末有空格，否则变成sWHERE
            "WHERE o.user_id = '${userId}' AND g.name LIKE '%${goodsName}%' AND o.goods_id = g.id AND g.shop_id = s.id " +
            "ORDER BY o.update_time DESC")
    List<OrdersGoodsShop> listByGoodsName(@Param("userId") String userId, @Param("goodsName") String goodsName);

    @Select("SELECT o.*, g.name as `goodsName`, g.category as `category`, g.tags, g.stock, g.image as `goodsImage`, "+
            "g.shop_id, s.name as `shopName`,s.image as `shopImage`, s.location as `shopLocation`, s.description as `shopDescription`, g.description as `goodsDescription` " +
            "FROM nm_orders o, nm_goods g, nm_shop s " +
            "WHERE s.id = '${shopId}' AND o.goods_id = g.id AND g.shop_id = s.id " +
            "ORDER BY o.update_time DESC")
    List<OrdersGoodsShop> listByShopId(@Param("shopId") String shopId);


    /**
     * 查询员工档期地址详情
     *
     * @param ordersId 订单id
     * @param consigneeName 收货人姓名
     * @param status 订单状态
     * @return
     */
    List<OrdersGoodsShop> listByIdOrConsigneeNameOrStatus(@Param("ordersId") String ordersId,
                                                          @Param("consigneeName") String consigneeName,
                                                          @Param("status") int status);

}
