package com.example.nutritionmall.example.service;
/**
 * @Author: Chris he
 * @Date: 2020/7/31 17:41
 */
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.nutritionmall.example.entity.NmOrders;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SecKillService extends IService<NmOrders> {

    List<Map<String, Object>> queryAllGoodStock();

    Map<String, Object> queryGoodStockById(Map<String, Object> m);

    List<Map<String, Object>> queryOrderByUserIdAndCoodsId(Map<String, Object> m);

//    Integer updateGoodStock(Map<String, Object> m);

//    Integer insertOrder(Map<String, Object> m);
}
