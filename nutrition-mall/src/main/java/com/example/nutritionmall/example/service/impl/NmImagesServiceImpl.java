package com.example.nutritionmall.example.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.example.entity.NmImages;
import com.example.nutritionmall.example.mapper.NmImagesMapper;
import com.example.nutritionmall.example.service.NmImagesService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NmImagesServiceImpl extends ServiceImpl<NmImagesMapper, NmImages> implements NmImagesService {

    @Override
    public Boolean add(NmImages images) {
        images.setCreateTime(DateTool.getCurrTime());
        this.save(images);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmImages images) {
        this.updateById(images);
        return true;
    }

    @Override
    public List<NmImages> getImagesByGoodsId(String goodsId) {
        QueryWrapper<NmImages> queryWrapper = new QueryWrapper<>();
        if(goodsId!=null){
            queryWrapper.like("goods_id", goodsId);
        }
        return this.list(queryWrapper);
    }

}
