package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmCoupon;

public interface NmCouponMapper extends BaseMapper<NmCoupon> {

}
