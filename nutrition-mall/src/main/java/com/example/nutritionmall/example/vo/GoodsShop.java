package com.example.nutritionmall.example.vo;

import com.example.nutritionmall.example.entity.NmGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "GoodsShop实体",description = "商品商店表")
public class GoodsShop extends NmGoods {
    @ApiModelProperty(value = "店铺名称")
    private String shopName;
    @ApiModelProperty(value = "店铺图片路径")
    private String shopImage;
}
