package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmCouponClaims;
import com.example.nutritionmall.example.service.NmCouponClaimsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/couponClaims")
@Api(tags = "NmCouponClaimsController", description = "类别controller，返回的baseUrl=http://localhost:8998/nutritionmall/")
public class NmCouponClaimsController {

    @Resource
    private NmCouponClaimsService nmCouponClaimsService;

    /**
     * 领取优惠券
     *
     * @param nmCouponClaims
     * @return
     */
    @ApiOperation(value = "领取优惠券")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmCouponClaims nmCouponClaims) {
        Result result = new Result();
        //同一用户只能领取一种优惠券一次
        NmCouponClaims couponExit = nmCouponClaimsService.isExit(nmCouponClaims.getUserId(),nmCouponClaims.getCouponId());
        if(couponExit != null){
            result.fail("优惠券已领取");
            return result;
        }
        nmCouponClaimsService.add(nmCouponClaims);
        result.success("领取成功");
        result.setData(nmCouponClaims);
        return result;
    }

    /**
     * 查询用户的优惠券信息
     * @param
     * @return
     */
    @ApiOperation(value = "查询用户的优惠券信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", paramType = "query", value = "用户id，可为空")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list(String userId) {
        Result result = new Result();
        result.setData(nmCouponClaimsService.list(userId));
        result.success("查询我的优惠券成功");
        return result;
    }
}
