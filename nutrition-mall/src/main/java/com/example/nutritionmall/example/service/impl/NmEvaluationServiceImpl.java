package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.JwtUtil;
import com.example.nutritionmall.example.entity.NmEvaluation;
import com.example.nutritionmall.example.mapper.NmEvaluationMapper;
import com.example.nutritionmall.example.service.NmEvaluationService;
import com.example.nutritionmall.example.vo.EvaluationOrdersGoodsShop;
import com.example.nutritionmall.example.vo.EvaluationUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class NmEvaluationServiceImpl extends ServiceImpl<NmEvaluationMapper, NmEvaluation> implements NmEvaluationService {

    @Autowired
    private HttpServletRequest request;

    @Override
    public Boolean add(NmEvaluation nmEvaluation) {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        nmEvaluation.setUserId(userId);
        nmEvaluation.setCreateTime(DateTool.getCurrTime());
        this.save(nmEvaluation);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmEvaluation nmEvaluation) {
        this.updateById(nmEvaluation);
        return true;
    }

    @Override
    public List<EvaluationOrdersGoodsShop> listByUserId() {
        String token = request.getHeader("Authorization");
        String userId = JwtUtil.validateToken(token);
        return baseMapper.listByUserId(userId);
    }

    @Override
    public Page<EvaluationUser> pageByGoodsId(String goodsId, Integer pageNum, Integer pageSize) {
        Page<EvaluationUser> page = new Page<>(pageNum, pageSize);
        return baseMapper.pageByGoodsId(page, goodsId);
    }

    @Override
    public NmEvaluation getByOrdersId(String ordersId) {
        QueryWrapper<NmEvaluation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("orders_id", ordersId);
        return this.getOne(queryWrapper);
    }

}
