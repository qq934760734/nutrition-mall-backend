package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmEvaluation;
import com.example.nutritionmall.example.service.NmEvaluationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/evaluation")
@Api(tags = "NmEvaluationController", description = "评价controller，返回的baseUrl=http://localhost:8998/petStore/")
public class NmEvaluationController {

    @Autowired
    private NmEvaluationService nmEvaluationService;

    //添加评价
    @ApiOperation(value = "添加评价(userId由后端从token中获取)")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmEvaluation nmEvaluation) {
        Result result = new Result();
        nmEvaluationService.add(nmEvaluation);
        result.success("添加成功");
        result.setData(nmEvaluation);
        return result;
    }

    //批量删除评价
    @ApiOperation(value = "批量删除评价")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmEvaluationService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    //修改评价
    @ApiOperation(value = "修改评价")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmEvaluation nmEvaluation) {
        Result result = new Result();
        nmEvaluationService.update(nmEvaluation);
        result.success("修改成功");
        return result;
    }

    //查询当前用户评价信息
    @ApiOperation(value = "查询当前用户评价信息")
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list() {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmEvaluationService.listByUserId());
        return result;
    }

    //通过商品id分页查询评价信息
    @ApiOperation(value = "通过商品id分页查询评价信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsId", required = true, paramType = "query", value = "需要查询评价的商品id"),
            @ApiImplicitParam(name = "pageNum", required = true, paramType = "query", value = "当前页数"),
            @ApiImplicitParam(name = "pageSize", required = true, paramType = "query", value = "每页条数")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/pageByGoodsId")
    public Result pageByGoodsId(String goodsId, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmEvaluationService.pageByGoodsId(goodsId, pageNum, pageSize));
        return result;
    }

    //查询指定评价id的评价信息
    @ApiOperation(value = "查询指定评价id的评价信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "evaluationId", required = true, paramType = "query", value = "需要查询的评价id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getById")
    public Result getById(String evaluationId) {
        Result result = new Result();
        result.success("查询成功");
        result.setData(nmEvaluationService.getById(evaluationId));
        return result;
    }

    //通过订单id查询评价信息
    @ApiOperation(value = "通过订单id查询评价信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ordersId", required = true, paramType = "query", value = "需要查询评价的商品id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getByOrdersId")
    public Result getByOrdersId(String ordersId) {
        Result result = new Result();
        result.success("查询评价成功");
        result.setData(nmEvaluationService.getByOrdersId(ordersId));
        return result;
    }

}
