package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmImages;
import com.example.nutritionmall.example.service.NmImagesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/images")
@Api(tags = "NmImagesController", description = "图片列表controller，返回的baseUrl=http://localhost:8998/bookStore/")
public class NmImagesController {

    @Resource
    private NmImagesService nmImagesService;

    //添加视频
    @ApiOperation(value = "添加图片")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmImages nmImages) {
        Result result = new Result();
        if (nmImages.getGoodsId() != null) {
            nmImagesService.add(nmImages);
            result.success("添加成功");
            result.setData(nmImages);
        } else {
            result.fail("添加失败，请返回重试");
        }
        return result;
    }

    //批量删除图片-deleteByIds
    @ApiOperation(value = "批量删除图片")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nmImages", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmImagesService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    //修改图片
    @ApiOperation(value = "修改图片")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmImages nmImages) {
        Result result = new Result();
        nmImagesService.update(nmImages);
        result.success("修改成功");
        return result;
    }

    //查-getById
    @ApiOperation(value = "通过id查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true, paramType = "query", value = "需要查询的id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getById")
    public Result getById(String id) {
        Result result = new Result();

        NmImages nmImages = nmImagesService.getById(id);
        if (nmImages != null) {
            result.success("获取成功");
            result.setData(nmImages);
        } else {
            result.fail("获取失败");
        }

        return result;
    }

    //根据goodsId查询images
    @ApiOperation(value = "通过goodsId查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goodsId", required = true, paramType = "query", value = "需要查询的图片id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getImagesByGoodsId")
    public Result getImagesByGoodsId(String goodsId) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmImagesService.getImagesByGoodsId(goodsId));
        return result;
    }

}
