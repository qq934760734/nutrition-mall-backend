package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NmUserMapper extends BaseMapper<NmUser> {

    /**
     * 根据请求搜索用户
     *
     * @param id
     * @param name
     * @param nickname
     * @param telephone
     * @param permission
     * @return
     */
    List<NmUser> listBySearch(@Param("uid") String id,
                              @Param("name") String name,
                              @Param("nickname") String nickname,
                              @Param("telephone") String telephone,
                              @Param("permission") String permission);
}
