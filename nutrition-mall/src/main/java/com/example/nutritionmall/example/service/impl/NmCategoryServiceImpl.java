package com.example.nutritionmall.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlLike;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.example.entity.NmCategory;
import com.example.nutritionmall.example.entity.NmUser;
import com.example.nutritionmall.example.mapper.NmCategoryMapper;
import com.example.nutritionmall.example.service.NmCategoryService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class NmCategoryServiceImpl extends ServiceImpl<NmCategoryMapper, NmCategory> implements NmCategoryService {

    @Override
    public Boolean add(NmCategory nmCategory) {
        nmCategory.setCreateTime(DateTool.getCurrTime());
        this.save(nmCategory);
        return true;
    }

    @Override
    public void deleteByIds(String ids) {
        List<String> listIds = new ArrayList<>();
        String[] aryIds = ids.split(",");
        for (String id : aryIds) {
            listIds.add(id);
        }
        this.removeByIds(listIds);
    }

    @Override
    public Boolean update(NmCategory nmCategory) {
        this.updateById(nmCategory);
        return true;
    }

    @Override
    public List<NmCategory> list(String name) {
        QueryWrapper<NmCategory> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", name);
        }
        return this.list(queryWrapper);
    }

    @Override
    public List<NmCategory> listByUserId(String createBy) {
        QueryWrapper<NmCategory> queryWrapper = new QueryWrapper<>();
        if (createBy != null) {
            queryWrapper.like("create_by", createBy);
        }
        return this.list(queryWrapper);
    }

    /**
     * 判断是否本人已经创建该分类
     * @param name
     * @param userName
     * @return
     */
    @Override
    public NmCategory isAlive(String name,String userName) {

        QueryWrapper<NmCategory> queryWrapper = new QueryWrapper<>();
        if(name!=null){
            queryWrapper.eq("name", name);
        }
        if(userName!=null){
            queryWrapper.eq("user_name",userName);
        }
        return this.getOne(queryWrapper);
    }

    @Override
    public List<NmCategory> listByName(String name) {
        QueryWrapper<NmCategory> queryWrapper = new QueryWrapper<>();
        if (name != null) {
            queryWrapper.like("name", "%"+name+"%");
        }
        return this.list(queryWrapper);
    }
}
