package com.example.nutritionmall.example;

import com.example.nutritionmall.common.base.BaseEntity;
import lombok.Data;



/**
 * 用来存储 搜索用户的实体
 */
@Data
public class UserRequest extends BaseEntity {
    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 用户权限，0-用户，1-商家，2-管理员
     */
    private String permission;
    /**
     * 联系方式
     */
    private String telephone;
}
