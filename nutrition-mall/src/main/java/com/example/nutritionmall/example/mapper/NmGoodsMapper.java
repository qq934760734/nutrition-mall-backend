package com.example.nutritionmall.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.nutritionmall.example.entity.NmGoods;
import com.example.nutritionmall.example.vo.GoodsShop;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface NmGoodsMapper extends BaseMapper<NmGoods> {

    @Select("SELECT g.*, s.name as `shop_name`, s.image as `shop_image` " +
            "FROM nm_goods g, nm_shop s " +
            "WHERE g.id = '${goodsId}' AND g.shop_id = s.id " +
            "ORDER BY g.create_time DESC")
    GoodsShop listByGoodsId(@Param("goodsId") String goodsId);

    List<NmGoods> listByIdOrGoodsNameOrCategory(@Param("goodsId") String goodsId,
                                                  @Param("goodsName") String goodsName,
                                                  @Param("category") String category);
    /**
     * 秒杀
     */
    Map<String, Object> queryGoodStockById(@Param("m") Map<String, Object> m);

    List<Map<String, Object>> queryAllGoodStock();

    List<Map<String, Object>> queryOrderByUserIdAndCoodsId(@Param("m") Map<String, Object> m);

}