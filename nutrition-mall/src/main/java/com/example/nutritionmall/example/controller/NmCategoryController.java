package com.example.nutritionmall.example.controller;

import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmCategory;
import com.example.nutritionmall.example.entity.NmUser;
import com.example.nutritionmall.example.service.NmCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@Slf4j
@RestController
@RequestMapping("/category")
@Api(tags = "NmCategoryController", description = "类别controller，返回的baseUrl=http://localhost:8998/nutritionmall/")
public class NmCategoryController {

    @Autowired
    private NmCategoryService nmCategoryService;

    //添加类别
    @ApiOperation(value = "添加类别")
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public Result add(@RequestBody NmCategory nmCategory) {
        Result result = new Result();
        NmCategory userNameExit = nmCategoryService.isAlive(nmCategory.getName(),nmCategory.getUserName());
        if (userNameExit != null) {
            result.fail("用户名:" + nmCategory.getName() + " 已存在");
            log.info("userNameExit为空");
        } else {
            log.info("userNameExit不为空");
            nmCategoryService.add(nmCategory);
            result.success("添加成功");
            result.setData(nmCategory);
        }
        return result;
    }

    //批量删除类别-deleteByIds
    @ApiOperation(value = "批量删除类别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, paramType = "query", value = "需要删除的多个id，用逗号,隔开")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/deleteByIds")
    public Result deleteByIds(String ids) {
        Result result = new Result();
        nmCategoryService.deleteByIds(ids);
        result.success("删除成功");
        return result;
    }

    //修改类别
    @ApiOperation(value = "修改类别")
    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public Result update(@RequestBody NmCategory nmCategory) {
        Result result = new Result();
        nmCategoryService.update(nmCategory);
        result.success("修改成功");
        return result;
    }

    //查询所有类别信息（可指定name）
    @ApiOperation(value = "查询所有类别信息（可指定name）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", paramType = "query", value = "类别关键字，可以为空"),
    })
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public Result list(String name) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmCategoryService.list(name));
        return result;
    }

    //查-getById
    @ApiOperation(value = "通过id查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true,paramType = "query",value = "需要查询的id")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/getById")
    public Result getById(String id) {
        Result result = new Result();
        NmCategory nmCategory = nmCategoryService.getById(id);
        if (nmCategory != null){
            result.success("获取成功");
            result.setData(nmCategory);
        }else{
            result.fail("获取失败");
        }
        return result;
    }

    //根据用户ID查询所有类别信息
    @ApiOperation(value = "查询所有类别信息")
    @RequestMapping(method = RequestMethod.POST, value = "/listByUserId")
    public Result listByUserId(String createBy) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmCategoryService.listByUserId(createBy));
        return result;
    }
    //模糊查找%name%类别信息
    @ApiOperation(value = "查询所有类别信息（可指定name）")
    @RequestMapping(method = RequestMethod.POST, value = "/listByName")
    public Result listByName(String name) {
        Result result = new Result();
        result.success("查询list成功");
        result.setData(nmCategoryService.listByName(name));
        return result;
    }
}
