package com.example.nutritionmall.common.utils.rabbitmq;

import com.example.nutritionmall.example.entity.NmOrders;
import com.example.nutritionmall.example.service.NmOrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
@RabbitListener(queues = "deadQueue")//监听的队列名称 deadQueue
@Slf4j
public class DeadReceiver {

    @Resource
    private NmOrdersService nmOrdersService;

    /**
     * 接收
     * @param message
     */
    @RabbitHandler
    public void process(Map message) {
        log.info("------RabbitMQ死信队列收到消息: " + message.toString());
        NmOrders nmOrders = nmOrdersService.getById(message.get("ordersId").toString());
        if (nmOrders != null && nmOrders.getStatus() == 0) {
            nmOrdersService.setStatus(nmOrders.getId(), -1, "超时未支付自动取消");
            log.info("------订单：" + nmOrders.getId() + "超时未支付自动取消");
        }
    }

}
