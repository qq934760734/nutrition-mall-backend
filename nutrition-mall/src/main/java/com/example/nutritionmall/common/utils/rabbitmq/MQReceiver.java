package com.example.nutritionmall.common.utils.rabbitmq;

/**
 * @Author: Chris he
 * @Date: 2020/7/31 17:36
 */

import com.example.nutritionmall.common.config.MQConfig;
import com.example.nutritionmall.common.utils.Result;
import com.example.nutritionmall.example.entity.NmGoods;
import com.example.nutritionmall.example.entity.NmOrders;
import com.example.nutritionmall.example.service.NmGoodsService;
import com.example.nutritionmall.example.service.NmOrdersService;
import com.example.nutritionmall.example.service.SecKillService;
import com.example.nutritionmall.example.service.impl.NmGoodsServiceImpl;
import com.example.nutritionmall.example.service.impl.SecKillServiceImpl;
import com.example.nutritionmall.example.vo.GoodsShop;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//消费者
@Service
@Slf4j
@RabbitListener(queues = "queue")//指明监听的是哪一个queue
public class MQReceiver {

    @Resource
    private NmGoodsService nmGoodsService;

    @Resource
    private NmOrdersService nmOrdersService;

    @RabbitHandler
    public void receive(Map<String, Object> msg) throws IOException {
        try {
            //log.info("监听到队列消息,用户id为：{}，商品id为：{},购买数量:{}", msg.get("user_id"),msg.get("goods_id"),msg.get("num"));
            int stock;
            //查数据库中商品库存
            List<GoodsShop> m = nmGoodsService.listByIds(msg.get("goods_id").toString());
            stock = m.get(0).getStock();
            log.warn("商品库存：{}", stock);
//        if (m != null && m.get("stock") != null) {
//            stock = Integer.parseInt(m.get("stock").toString());
//        }
            if (stock <= 0) {//库存不足
                log.warn("用户：{}秒杀时商品的库存量没有剩余,秒杀结束", msg.get("user_id"));
                return;
            }
            if(stock-Integer.parseInt(msg.get("quantity").toString())<0){
                log.warn("商品：{}，库存不足", msg.get("goodsId"));
                return;
            }
            //这里业务是同一用户同一商品只能购买一次,所以判断该商品用户是否下过单
//        List<Map<String, Object>> list = miaoshaService.queryOrderByUserIdAndCoodsId(msg);
//        if(list != null && list.size() > 0){//重复下单
//            return;
//        }

            //减库存
            // 创建一个Map对象
            Map<String, Integer> stockMap = new HashMap<>();
            stockMap.put(m.get(0).getId(), Integer.parseInt(msg.get("quantity").toString())); // 产品A的库存是100
            String operator = "reduce";
            //更新库存
            nmGoodsService.updateStockByMap(stockMap, operator);
            //下订单
            log.info("用户：{}秒杀该商品：{}库存有余:{},可以进行下订单操作", msg.get("user_id"), msg.get("goods_id"), stock);
            NmOrders nmOrders = new NmOrders();
            nmOrders.setUserId(msg.get("user_id").toString());
            nmOrders.setGoodsId(msg.get("goods_id").toString());
            nmOrders.setQuantity(Integer.parseInt(msg.get("quantity").toString()));
            nmOrders.setAddress(msg.get("address").toString());
            nmOrders.setShopId(msg.get("shop_id").toString());
            nmOrders.setTelephone(msg.get("telephone").toString());
            nmOrders.setConsigneeName(msg.get("consignee_name").toString());
            nmOrders.setTotalPrice(Double.parseDouble(msg.get("total_price").toString()));
            nmOrders.setPrice(Double.parseDouble(msg.get("price").toString()));
            nmOrders.setStatus(1);//秒杀一定要先付款
            if (nmOrders == null) {
                System.out.println("nmOrders is null");
            } else {
                //添加订单
                //将生成的订单放入缓存
                nmOrdersService.secKillAdd(nmOrders);
                log.info("添加成功");
            }
        } catch (Exception e) {
            log.warn(String.valueOf(e));
        }
    }

}
