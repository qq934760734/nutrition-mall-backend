package com.example.nutritionmall.common.utils.rabbitmq;

import com.example.nutritionmall.common.config.MQConfig;
import com.example.nutritionmall.common.utils.DateTool;
import com.example.nutritionmall.common.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@Slf4j
public class SendMessageUtil {

    @Resource
    private RabbitTemplate rabbitTemplate;   //使用RabbitTemplate,这提供了接收/发送等等方法

    // 发送消息
    public Result sendDirectMessage(Map messageMap) {
        Result result = new Result();
        //将要发送的消息转为map
        Map<String, Object> map = new HashMap<>(messageMap);
        String messageId = String.valueOf(UUID.randomUUID());
        String createTime = DateTool.getCurrTime();
        map.put("messageId", messageId);
        map.put("createTime", createTime);
        //将消息携带绑定键值：directRouting 发送到交换机directExchange
        rabbitTemplate.convertAndSend("directExchange", "directRouting", map);
        log.info("------RabbitMQ发送消息  : " + map);
        result.success("RabbitMq发送成功");
        result.setData(map);
        return result;
    }


}