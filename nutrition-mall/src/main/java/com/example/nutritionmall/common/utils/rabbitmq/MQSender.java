package com.example.nutritionmall.common.utils.rabbitmq;

/**
 * @Author: Chris he
 * @Date: 2020/7/31 17:35
 */

import com.example.nutritionmall.common.config.MQConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

//生产者
@Service
@Slf4j
public class MQSender {

    @Autowired
    AmqpTemplate amqpTemplate;

//    //Direct模式
//    public void send(Map<String,Object> msg) {
//        //第一个参数队列的名字，第二个参数发出的信息
//        amqpTemplate.convertAndSend(MQConfig.QUEUE, msg);
//    }
    //Direct模式
    public void send(Map<String,Object> msg) {
        //第一个参数队列的名字，第二个参数发出的信息
        amqpTemplate.convertAndSend(MQConfig.QUEUE,  msg);
        log.info("------RabbitMQ发送消息  : " + msg);
    }
}
